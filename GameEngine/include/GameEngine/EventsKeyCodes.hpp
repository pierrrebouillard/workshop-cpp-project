/**
 * @file EventsKeys.hpp
 * @brief List of events keycodes
 * @author curs3w4ll
 * @version 1
 */

#pragma once

namespace ge {

/**
 * @enum EventsKeys
 * @brief Codes of user related events
 */
enum class EventsKeys {
    Undefined = 0, // Undefined event
    Closed, // The event when the window is closed
    Resized, // The event when the window is resized
    LostFocus, // The event when window lose focus
    GainedFocus, // The event when window gain focus
    KeyboardPressed, // The event when a keyboard key is pressed
    KeyboardReleased, // The event when a keyboard key is released

    MAX, // Keep last -- the total number of keycodes
};

/**
 * @enum KeyboardKeys
 * @brief Codes of keyboard keys
 */
enum class KeyboardKeys {
    Undefined = 0, // Not supported key
    A, // The keyboard A key
    B, // The keyboard B key
    C, // The keyboard C key
    D, // The keyboard D key
    E, // The keyboard E key
    F, // The keyboard F key
    G, // The keyboard G key
    H, // The keyboard H key
    I, // The keyboard I key
    J, // The keyboard J key
    K, // The keyboard K key
    L, // The keyboard L key
    M, // The keyboard M key
    N, // The keyboard N key
    O, // The keyboard O key
    P, // The keyboard P key
    Q, // The keyboard Q key
    R, // The keyboard R key
    S, // The keyboard S key
    T, // The keyboard T key
    U, // The keyboard U key
    V, // The keyboard V key
    W, // The keyboard W key
    X, // The keyboard X key
    Y, // The keyboard Y key
    Z, // The keyboard Z key
    Num0, // The keyboard 0 key
    Num1, // The keyboard 1 key
    Num2, // The keyboard 2 key
    Num3, // The keyboard 3 key
    Num4, // The keyboard 4 key
    Num5, // The keyboard 5 key
    Num6, // The keyboard 6 key
    Num7, // The keyboard 7 key
    Num8, // The keyboard 8 key
    Num9, // The keyboard 9 key
    Escape, // The keyboard Escape key
    LControl, // The keyboard left Control key
    LShift, // The keyboard left Shift key
    LAlt, // The keyboard left Alt key
    LSystem, // The keyboard left OS specific key: window (Windows and Linux), apple (MacOS X), ...
    RControl, // The keyboard right Control key
    RShift, // The keyboard right Shift key
    RAlt, // The keyboard right Alt key
    RSystem, // The keyboard right OS specific key: window (Windows and Linux), apple (MacOS X), ...
    Menu, // The keyboard Menu key
    LBracket, // The keyboard [ key
    RBracket, // The keyboard ] key
    Semicolon, // The keyboard ; key
    Comma, // The keyboard , key
    Period, // The keyboard . key
    Quote, // The keyboard ' key
    Slash, // The keyboard / key
    Backslash, // The keyboard \ key
    Tilde, // The keyboard ~ key
    Equal, // The keyboard = key
    Hyphen, // The keyboard - key (hyphen)
    Space, // The keyboard Space key
    Enter, // The keyboard Enter/Return keys
    Backspace, // The keyboard Backspace key
    Tab, // The keyboard Tabulation key
    PageUp, // The keyboard Page up key
    PageDown, // The keyboard Page down key
    End, // The keyboard End key
    Home, // The keyboard Home key
    Insert, // The keyboard Insert key
    Delete, // The keyboard Delete key
    Add, // The keyboard + key
    Subtract, // The keyboard - key (minus, usually from numpad)
    Multiply, // The keyboard * key
    Divide, // The keyboard / key
    Left, // The keyboard Left arrow
    Right, // The keyboard Right arrow
    Up, // The keyboard Up arrow
    Down, // The keyboard Down arrow
    Numpad0, // The keyboard numpad 0 key
    Numpad1, // The keyboard numpad 1 key
    Numpad2, // The keyboard numpad 2 key
    Numpad3, // The keyboard numpad 3 key
    Numpad4, // The keyboard numpad 4 key
    Numpad5, // The keyboard numpad 5 key
    Numpad6, // The keyboard numpad 6 key
    Numpad7, // The keyboard numpad 7 key
    Numpad8, // The keyboard numpad 8 key
    Numpad9, // The keyboard numpad 9 key
    F1, // The keyboard F1 key
    F2, // The keyboard F2 key
    F3, // The keyboard F3 key
    F4, // The keyboard F4 key
    F5, // The keyboard F5 key
    F6, // The keyboard F6 key
    F7, // The keyboard F7 key
    F8, // The keyboard F8 key
    F9, // The keyboard F9 key
    F10, // The keyboard F10 key
    F11, // The keyboard F11 key
    F12, // The keyboard F12 key
    F13, // The keyboard F13 key
    F14, // The keyboard F14 key
    F15, // The keyboard F15 key
    Pause, // The keyboard Pause key

    MAX, // Keep last -- the total number of keycodes
};

struct Event {
    explicit Event(EventsKeys eventKey = EventsKeys::Undefined, KeyboardKeys keyboardKey = KeyboardKeys::Undefined) noexcept :
        eventKey(eventKey), keyboardKey(keyboardKey) { }
    EventsKeys eventKey;
    KeyboardKeys keyboardKey;
};

} // namespace ge
