/**
 * @file Window.hpp
 * @brief Game engine window class encapsulation
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "EventsKeyCodes.hpp"
#include "EventsListener.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/System/String.hpp"
#include "Utils/Vector.hpp"

#include <string>
#include <vector>

namespace ge {

namespace ecs::system {
    class ImageRenderSystem;
    class TextRenderSystem;
} // namespace ecs::system

/**
 * @class Window
 * @brief Encapsulated graphic window
 */
class Window {
    friend class ecs::system::ImageRenderSystem;
    friend class ecs::system::TextRenderSystem;

   public:
    Window() noexcept = default;
    /**
     * @brief Init the window
     *
     * @warning Using this constructor will make the newly created window use the default desktop parameters
     *
     * @param title The title of the window
     */
    explicit Window(const std::string& title) noexcept;
    /**
     * @brief Init the window
     *
     * @param width The width of the window
     * @param height The height of the window
     * @param title The title of the window
     * @param bitsPerPixel The number of bits a unique pixel is constitued of
     */
    Window(unsigned int width, unsigned int height, const std::string& title = "Window", unsigned int bitsPerPixel = 32) noexcept;
    Window(Window const& other) noexcept = delete;
    Window(Window&& other) noexcept = delete;
    ~Window() noexcept = default;

    Window& operator=(Window const& other) noexcept = delete;
    Window& operator=(Window&& other) noexcept = delete;

    /**
     * @brief Init the graphic window
     * @details If the window has already been created, the previous one will be destroyed
     *
     * @param title The title of the window
     */
    void init(const std::string& title = "Window") noexcept;
    /**
     * @brief Init the graphic window
     * @details If the window has already been created, the previous one will be destroyed
     *
     * @param width The width of the window
     * @param height The height of the window
     * @param title The title of the window
     * @param bitsPerPixel The number of bits a unique pixel is constitued of
     */
    void init(unsigned int width, unsigned int height, const std::string& title = "Window", unsigned int bitsPerPixel = 32) noexcept;
    /**
     * @brief Close the graphic window
     *
     * @pre This function should be called only if isOpen return true
     */
    void close() noexcept;
    /**
     * @brief Check if the window is openned
     *
     * @return `false` if the window is closed, `true` otherwise
     */
    bool isOpen() const noexcept;
    /**
     * @brief Clear the window
     */
    void clear() noexcept;
    /**
     * @brief Display elements on the window
     */
    void display() noexcept;
    /**
     * @brief getEvents
     *
     * @return A vector of the different events that occured in the window since the last call to this function
     */
    std::vector<ge::Event> getEvents() noexcept;
    /**
     * @brief Get the size of the window
     *
     * @return A vector that contain the size of the window (x is width and y is height)
     */
    utils::Vector getSize() const noexcept;

   private:
    /**
     * @var _inited
     * @brief Set to `true` if the window has been inited
     */
    bool _inited = false;
    /**
     * @var window
     * @brief The SFML window
     */
    sf::RenderWindow window;
    /**
     * @var eventsListener
     * @brief The event listener class related to used graphic library
     */
    EventsListener eventsListener;
};

} // namespace ge
