/**
 * @file Transform.hpp
 * @brief Transform component to set the transformation Entity
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/Utils/Vector.hpp"

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#pragma once

namespace ge::ecs::component {

/**
 * @struct Transform
 * @brief Transform contains different variable for the transformation Entity
 */
struct Transform {
    /**
     * @var movement
     * @brief movement indicate movement for the Entity
     */
    utils::Vector movement{0, 0};
    /**
     * @var grow
     * @brief grow indicate grow for the Entity
     */
    utils::Vector grow{0, 0};
    /**
     * @var rotation
     * @brief rotation direction speed for the Entity
     */
    float rotation = 0;
};

} // namespace ge::ecs::component