/**
 * @file Collider.hpp
 * @brief ECS collision callbacks
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "GameEngine/ECS/Core/Entity.hpp"
#include "GameEngine/Utils/Callback.hpp"
#include "GameEngine/Utils/Utils.hpp"

#include <vector>

namespace ge::ecs::component {

/**
 * @class Collider
 * @brief Collider managing callbacks
 */
class Collider {
   public:
    Collider() noexcept = delete;
    /**
     * @brief Collider type creator template
     *
     * @tparam Args The different collidertypes constituing the final type
     */
    template <typename... Args>
    /**
     * @brief Create a new collider function
     *
     * @param args The collider types
     */
    explicit Collider(Args... args) noexcept
    {
        ((this->type |= static_cast<int>(args)), ...);
    }
    Collider(Collider const& other) noexcept = delete;
    Collider(Collider&& other) noexcept = default;
    ~Collider() noexcept = default;

    Collider& operator=(Collider const& other) noexcept = delete;
    Collider& operator=(Collider&& other) noexcept = default;

    /**
     * @brief Get the type of the collider
     *
     * @return The collider composition type
     */
    int getType() const noexcept;
    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Set the callback for collision
     * @warning Calling this function multiple times will override the previously setted callback
     * @pre The function pointer passed here (f) should take a reference to the entity it collide with and a reference to the other collider at the end of the arguments
     *
     * @param f The function to use as callback
     * @param args The arguments to capture and pass to the function firstly
     */
    void setCallback(F&& f, Args&&... args) noexcept
    {
        this->callback.define(std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Remove the existing callback
     */
    void clearCallback() noexcept;
    /**
     * @brief Run the callback for the given entity and its associated collider
     *
     * @param other The entity its colliding with
     * @param otherCollider The collider its colliding with
     * @param collidingFaces The number of pixel each face is colliding
     *
     * @throw Anything the callback could throw
     */
    void runCallback(ecs::core::Entity const& other, Collider const& otherCollider, ge::utils::Rect<float> collidingFaces);

   private:
    /**
     * @var CallbackSignature
     * @brief The signature of the colliders callbacks functions
     */
    using CallbackSignature = void(ecs::core::Entity const&, Collider const&, ge::utils::Rect<float>);

    /**
     * @var type
     * @brief Collider type of the collider
     */
    int type = 0;
    /**
     * @var callback
     * @brief The function to call when a collision occure
     */
    ge::utils::Callback<CallbackSignature> callback{};
};

} // namespace ge::ecs::component
