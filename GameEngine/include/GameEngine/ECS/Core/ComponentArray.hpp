/**
 * @file ComponentArray.hpp
 * @brief Components storage for ECS
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "Entity.hpp"

#include <optional>
#include <vector>

namespace ge::ecs::core {

/**
 * @class IComponentArray
 * @brief ECS componentArray interface
 * @details Used to store componentsArray no matter what is the type of the component
 */
class IComponentArray {
   public:
    IComponentArray() noexcept = default;
    IComponentArray(IComponentArray const& other) noexcept = delete;
    IComponentArray(IComponentArray&& other) noexcept = default;
    virtual ~IComponentArray() noexcept = default;

    IComponentArray& operator=(IComponentArray const& other) noexcept = delete;
    IComponentArray& operator=(IComponentArray&& other) noexcept = default;
};

/**
 * @brief Generate a ComponentArray according to the type gived in the template
 *
 * @tparam T The type of the component to generate an array for
 */
template <typename T>
/**
 * @class ComponentArray
 * @brief Generated an object to interact with an array of T type component
 */
class ComponentArray final : public IComponentArray {
   public:
    /**
     * @brief Type of the value stored in the array
     */
    using value_type = std::optional<T>;
    /**
     * @brief Reference type of the value stored
     */
    using reference_type = value_type&;
    /**
     * @brief Constant reference type of the value stored
     */
    using const_reference_type = value_type const&;
    /**
     * @brief Type of the object used to store the T component array
     */
    using container_type = std::vector<value_type>;
    /**
     * @brief The type used for the type of the container
     */
    using size_type = typename container_type::size_type;
    /**
     * @brief The type used for container iterators
     */
    using iterator = typename container_type::iterator;
    /**
     * @brief The reference type used for container iterators
     */
    using const_iterator = typename container_type::const_iterator;

    ComponentArray() noexcept = default;
    ComponentArray(ComponentArray const& other) noexcept = delete;
    ComponentArray(ComponentArray&& other) noexcept = default;
    ~ComponentArray() noexcept final = default;

    ComponentArray& operator=(ComponentArray const& other) noexcept = delete;
    ComponentArray& operator=(ComponentArray&& other) noexcept = default;

    /**
     * @brief Access the component associated to the given entity
     * @details If the component does not exist, return nullopt
     *
     * @param entity The entity to get the component associated with
     *
     * @return A reference to the component
     */
    reference_type operator[](Entity const& entity)
    {
        if (entity >= this->components.size()) {
            return this->expandToIndex(entity);
        } else {
            return this->components[entity];
        }
    }
    const_reference_type operator[](Entity const& entity) const
    {
        if (entity >= this->components.size()) {
            return this->expandToIndex(entity);
        } else {
            return this->components[entity];
        }
    }
    reference_type operator[](std::size_t entity)
    {
        if (entity >= this->components.size()) {
            return this->expandToIndex(entity);
        } else {
            return this->components[entity];
        }
    }
    const_reference_type operator[](std::size_t entity) const
    {
        if (entity >= this->components.size()) {
            return this->expandToIndex(entity);
        } else {
            return this->components[entity];
        }
    }

    /**
     * @brief Get an iterator to the begining of the array
     *
     * @return An iterator to the first element of the array
     */
    iterator begin() noexcept { return this->components.begin(); }
    /**
     * @brief Get a constant iterator to the biegining of the array
     * @details This function will be called only in const scopes
     *
     * @return A constant iterator to the first element of the array
     */
    const_iterator begin() const noexcept { return this->components.begin(); }
    /**
     * @brief Get a constant iterator to the biegining of the array
     *
     * @return A constant iterator to the first element of the array
     */
    const_iterator cbegin() const noexcept { return this->components.cbegin(); }
    /**
     * @brief Get an iterator to the end of the array
     *
     * @return An iterator to the last element of the array
     */
    iterator end() noexcept { return this->components.end(); }
    /**
     * @brief Get a constant iterator to the end of the array
     * @details This function will be called only in const scopes
     *
     * @return A constant iterator to the last element of the array
     */
    const_iterator end() const noexcept { return this->components.end(); }
    /**
     * @brief Get a constant iterator to the end of the array
     *
     * @return A constant iterator to the last element of the array
     */
    const_iterator cend() const noexcept { return this->components.cend(); }
    /**
     * @brief Get the size of the array
     *
     * @return The number of elements in the array (the ones difined to nullopt count)
     */
    size_type size() const noexcept { return this->components.size(); }
    reference_type insert_at(Entity const& entity, T const& component)
    {
        if (entity >= this->components.size()) {
            this->expandToIndex(entity);
        }
        this->components[entity] = component;
        return this->components[entity];
    }
    /**
     * @brief Insert a component for the given entity
     *
     * @pre The component passed to this function need to already have been constructed
     *
     * @param entity The entity to set the component for
     * @param component The component to set for the entity
     *
     * @return A reference to the stored component that you can modify
     */
    reference_type insert_at(Entity const& entity, T&& component)
    {
        if (entity >= this->components.size()) {
            this->expandToIndex(entity);
        }
        this->components[entity] = std::move(component);
        return this->components[entity];
    }
    /**
     * @brief Component templated set by construction
     *
     * @tparam Params The parameters type to pass to the component constructor
     */
    template <class... Params>
    /**
     * @brief Insert and construct a component for the given entity
     *
     * @param entity The entity to set the component for
     * @param params The parameters that will be passed to the component constructor
     *
     * @return A reference to the stored component that you can modify
     */
    reference_type emplace_at(Entity const& entity, Params&&... params)
    {
        return this->insert_at(entity, T(std::forward<Params>(params)...));
    }
    /**
     * @brief Destroy the component of the given entity
     * @warning This will make all the references to this component invalid
     *
     * @param entity The entity to destroy the component linked to
     */
    void erase(Entity const& entity) noexcept
    {
        if (entity < this->components.size()) {
            this->components[entity] = std::nullopt;
        }
    }

   private:
    /**
     * @brief Expand the array so it can contain the given index
     *
     * @param idx The index to ensure it fit in the array
     *
     * @return A reference to the last element of the array
     */
    reference_type expandToIndex(std::size_t idx) noexcept
    {
        this->components.resize(idx + 1);
        return this->components.back();
    }
    /**
     * @brief The array that stock components
     */
    container_type components;
};

} // namespace ge::ecs::core
