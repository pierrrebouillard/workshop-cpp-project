/**
 * @file AnimationSystem.hpp
 * @author Baptiste-MV
 * @brief Animation suystem implementation
 * @version 1
 *
 */

#pragma once

#include "GameEngine/ECS/Core/ASystem.hpp"
#include "GameEngine/ECS/Core/Coordinator.hpp"

namespace ge::ecs::system {

/**
 * @class AnimationSystem
 * @brief The ECS system animate object
 */
class AnimationSystem final : public core::ASystem {
   public:
    AnimationSystem() noexcept = delete;
    explicit AnimationSystem(ecs::core::Coordinator& coordinator) noexcept;
    AnimationSystem(AnimationSystem const& other) noexcept = delete;
    AnimationSystem(AnimationSystem&& other) noexcept = default;
    ~AnimationSystem() noexcept final = default;

    AnimationSystem& operator=(AnimationSystem const& other) noexcept = delete;
    AnimationSystem& operator=(AnimationSystem&& other) noexcept = delete;

    /**
     * @brief update animation and frame
     *
     * @param elaspedTime the time passed to update
     */
    void update(float elaspedTime);
};

} // namespace ge::ecs::system
