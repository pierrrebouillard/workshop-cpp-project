/**
 * @file CollisionSystem.hpp
 * @brief Entity Bounds system definition
 * @author Baptiste-MV
 * @version 1
 */

#pragma once

#include "GameEngine/ECS/Components/Bounds.hpp"
#include "GameEngine/ECS/Components/Transform.hpp"
#include "GameEngine/ECS/Core/ASystem.hpp"
#include "GameEngine/ECS/Core/Coordinator.hpp"
#include "GameEngine/ECS/Core/Entity.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string_view>

namespace ge::ecs::system {

/**
 * @class CollisionSystem
 * @brief The ECS system check bounds entity to functions
 */
class CollisionSystem final : public core::ASystem {
   public:
    CollisionSystem() noexcept = delete;
    explicit CollisionSystem(ecs::core::Coordinator& coordinator) noexcept;
    CollisionSystem(CollisionSystem const& other) noexcept = delete;
    CollisionSystem(CollisionSystem&& other) noexcept = default;
    ~CollisionSystem() noexcept final = default;

    CollisionSystem& operator=(CollisionSystem const& other) noexcept = delete;
    CollisionSystem& operator=(CollisionSystem&& other) noexcept = delete;

    /**
     * @brief Check collision for entities
     *
     * @param i The entity we start checking for in the first dimension
     * @param index2 The entity we start checking for in the second dimension
     */
    void update(std::size_t i = 0, std::size_t index2 = 1, std::set<std::size_t> doneY = {});
};

} // namespace ge::ecs::system