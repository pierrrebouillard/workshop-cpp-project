/**
 * @file TextBoundsSystem.hpp
 * @brief Bounds of text entity system definition
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/ECS/Components/Bounds.hpp"
#include "GameEngine/ECS/Components/RenderableText.hpp"
#include "GameEngine/ECS/Core/ASystem.hpp"
#include "GameEngine/ECS/Core/Coordinator.hpp"
#include "GameEngine/ECS/Core/Entity.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <string_view>

#pragma once

namespace ge::ecs::system {

/**
 * @class TextBoundsSystem
 * @brief The ECS system bounds for texts
 */
class TextBoundsSystem final : public core::ASystem {
   public:
    TextBoundsSystem() noexcept = delete;
    explicit TextBoundsSystem(ecs::core::Coordinator& coordinator) noexcept;
    TextBoundsSystem(TextBoundsSystem const& other) noexcept = delete;
    TextBoundsSystem(TextBoundsSystem&& other) noexcept = default;
    ~TextBoundsSystem() noexcept final = default;

    TextBoundsSystem& operator=(TextBoundsSystem const& other) noexcept = delete;
    TextBoundsSystem& operator=(TextBoundsSystem&& other) noexcept = delete;

    /**
     * @brief update the bounds of the texts
     *
     * @param coordinator The ECS coordinator
     */
    void update();
};

} // namespace ge::ecs::system