/**
 * @file AreaBoundsSystem.hpp
 * @brief Bounds of Area entity system definition
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/ECS/Core/ASystem.hpp"
#include "GameEngine/ECS/Core/Coordinator.hpp"
#include "GameEngine/ECS/Core/Entity.hpp"

#pragma once

namespace ge::ecs::system {

/**
 * @class AreaBoundsSystem
 * @brief The ECS system bounds for Areas
 */
class AreaBoundsSystem final : public core::ASystem {
   public:
    AreaBoundsSystem() noexcept = delete;
    explicit AreaBoundsSystem(ecs::core::Coordinator& coordinator) noexcept;
    AreaBoundsSystem(AreaBoundsSystem const& other) noexcept = delete;
    AreaBoundsSystem(AreaBoundsSystem&& other) noexcept = default;
    ~AreaBoundsSystem() noexcept final = default;

    AreaBoundsSystem& operator=(AreaBoundsSystem const& other) noexcept = delete;
    AreaBoundsSystem& operator=(AreaBoundsSystem&& other) noexcept = delete;

    /**
     * @brief update the bounds of the Areas
     */
    void update();
};

} // namespace ge::ecs::system