/**
 * @file ImageRenderSystem.hpp
 * @brief Render of images system definition
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/ECS/Core/ASystem.hpp"
#include "GameEngine/ECS/Core/Coordinator.hpp"
#include "GameEngine/Window.hpp"

#pragma once

namespace ge::ecs::system {

/**
 * @class ImageRenderSystem
 * @brief The ECS system display image to functions
 */
class ImageRenderSystem final : public core::ASystem {
   public:
    ImageRenderSystem() noexcept = delete;
    explicit ImageRenderSystem(ecs::core::Coordinator& coordinator) noexcept;
    ImageRenderSystem(ImageRenderSystem const& other) noexcept = delete;
    ImageRenderSystem(ImageRenderSystem&& other) noexcept = default;
    ~ImageRenderSystem() noexcept final = default;

    ImageRenderSystem& operator=(ImageRenderSystem const& other) noexcept = delete;
    ImageRenderSystem& operator=(ImageRenderSystem&& other) noexcept = delete;

    /**
     * @brief update position, scale, rotation and display sprite image
     *
     * @param coordinator The ECS coordinator
     *
     * @param window The window of game
     */
    void update(Window& window);
};

} // namespace ge::ecs::system