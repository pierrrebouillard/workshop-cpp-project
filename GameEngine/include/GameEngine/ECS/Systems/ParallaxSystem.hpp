/**
 * @file ParallaxSystem.hpp
 * @brief Render of images system definition
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/ECS/Core/ASystem.hpp"
#include "GameEngine/ECS/Core/Coordinator.hpp"

#pragma once

namespace ge::ecs::system {

/**
 * @class ParallaxSystem
 * @brief The ECS system display image to functions
 */
class ParallaxSystem final : public core::ASystem {
   public:
    ParallaxSystem() noexcept = delete;
    explicit ParallaxSystem(ecs::core::Coordinator& coordinator) noexcept;
    ParallaxSystem(ParallaxSystem const& other) noexcept = delete;
    ParallaxSystem(ParallaxSystem&& other) noexcept = default;
    ~ParallaxSystem() noexcept final = default;

    ParallaxSystem& operator=(ParallaxSystem const& other) noexcept = delete;
    ParallaxSystem& operator=(ParallaxSystem&& other) noexcept = delete;

    /**
     * @brief update position, scale, rotation and display sprite image
     *
     * @param coordinator The ECS coordinator
     */
    void update();
};

} // namespace ge::ecs::system