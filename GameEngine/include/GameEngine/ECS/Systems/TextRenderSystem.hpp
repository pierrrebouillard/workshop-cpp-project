/**
 * @file TextRenderSystem.hpp
 * @brief Render of texts system definition
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/ECS/Core/ASystem.hpp"
#include "GameEngine/ECS/Core/Coordinator.hpp"
#include "GameEngine/Window.hpp"

#pragma once

namespace ge::ecs::system {

/**
 * @class TextRenderSystem
 * @brief The ECS system display text to functions
 */
class TextRenderSystem final : public core::ASystem {
   public:
    TextRenderSystem() noexcept = delete;
    explicit TextRenderSystem(ecs::core::Coordinator& coordinator) noexcept;
    TextRenderSystem(TextRenderSystem const& other) noexcept = delete;
    TextRenderSystem(TextRenderSystem&& other) noexcept = default;
    ~TextRenderSystem() noexcept final = default;

    TextRenderSystem& operator=(TextRenderSystem const& other) noexcept = delete;
    TextRenderSystem& operator=(TextRenderSystem&& other) noexcept = delete;

    /**
     * @brief update position, scale, rotation and display sprite text
     *
     * @param coordinator The ECS coordinator
     *
     * @param window The window of game
     */
    void update(Window& window);
};

} // namespace ge::ecs::system