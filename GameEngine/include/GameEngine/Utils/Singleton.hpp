/**
 * @file Singleton.hpp
 * @brief Unique instanciable object
 * @author curs3w4ll
 * @version 1
 */

#pragma once

/**
 * @namespace ge::utils
 * @brief Utilities for game engine
 */
namespace ge::utils {

/**
 * @brief Templated singleton type
 *
 * @tparam T The object type to make singleton for
 */
template <typename T>
/**
 * @class Singleton
 * @brief Singleton implementation
 * @warning Use this class when you want your object to be a singleton<br/>
 * To make your class invokable, you need to declare a constructor with this form:<br/>
 * **Where T is your class type**
 * `explicit T(token t) : Singleton(t) { // your code }`<br/>
 * Then you can use the function getInstance() inside your methods to get the unique instance
 *
 * @see GameEngine for implementation example
 */
class Singleton {
   public:
    Singleton() noexcept = delete;
    Singleton(Singleton const& other) noexcept = delete;
    Singleton(Singleton&& other) noexcept = delete;
    ~Singleton() noexcept = default;

    Singleton& operator=(Singleton const& other) noexcept = delete;
    Singleton& operator=(Singleton&& other) noexcept = delete;

   protected:
    struct token { };
    explicit Singleton(token /*unused*/) noexcept {};
    static T& getInstance()
    {
        static T instance{token{}};

        return instance;
    }
};

} // namespace ge::utils
