/*
** EPITECH PROJECT, 2022
** R-Type
** File description:
** InstallFilePaths
*/

#pragma once

#include <string_view>

#if defined(_WIN32) or defined(_WIN64)
constexpr std::string_view ASSETSPATH = "../assets";
#else
constexpr std::string_view ASSETSPATH = "./assets";
#endif