/**
 * @file EventsHandler.hpp
 * @brief Handle events and do actions according
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "EventsKeyCodes.hpp"
#include "GameEngine/Utils/Callback.hpp"

#include <array>
#include <vector>

namespace ge {

/**
 * @class EventsHandler
 * @brief Handle events and do actions according to the user definitions
 */
class EventsHandler {
   public:
    EventsHandler() noexcept = default;
    EventsHandler(EventsHandler const& other) noexcept = delete;
    EventsHandler(EventsHandler&& other) noexcept = delete;
    ~EventsHandler() noexcept = default;

    EventsHandler& operator=(EventsHandler const& other) noexcept = delete;
    EventsHandler& operator=(EventsHandler&& other) noexcept = delete;

    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a callback function that will be called when a key get pressed
     *
     * @param key The keyboard key to associate callback to
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setPressedKeyCallback(ge::KeyboardKeys key, F&& f, Args&&... args) noexcept
    {
        this->pressedCallbacks.at(static_cast<std::size_t>(key)).define(std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Clear existing pressed callbacks associated to the given key
     *
     * @warning This will clear ALL existing callbacks
     */
    void clearPressedKeyCallbacks() noexcept;
    /**
     * @brief Clear existing pressed callback associated to the given key
     *
     * @param key The key to delete the associated callback
     */
    void clearPressedKeyCallbackFor(ge::KeyboardKeys key) noexcept;
    /**
     * @brief Clear existing pressed callback associated to the given keys
     *
     * @param keys A list of keys to delete the associated callbacks
     */
    void clearPressedKeyCallbackFor(std::initializer_list<ge::KeyboardKeys> keys) noexcept;

    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a callback function that will be called when a key get pressed
     *
     * @param key The keyboard key to associate callback to
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setReleasedKeyCallback(ge::KeyboardKeys key, F&& f, Args&&... args) noexcept
    {
        this->releasedCallbacks.at(static_cast<std::size_t>(key)).define(std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Clear all existing released callbacks
     *
     * @warning This will clear ALL existing callbacks
     */
    void clearReleasedKeyCallbacks() noexcept;
    /**
     * @brief Clear existing released callback associated to the given key
     *
     * @param key The key to delete the associated callback
     */
    void clearReleasedKeyCallbackFor(ge::KeyboardKeys key) noexcept;
    /**
     * @brief Clear existing released callback associated to the given keys
     *
     * @param keys A list of keys to delete the associated callbacks
     */
    void clearReleasedKeyCallbackFor(std::initializer_list<ge::KeyboardKeys> keys) noexcept;

    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a callback function that will be called when the given event occurs
     *
     * @param event The event to associate callback to
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setEventCallback(ge::EventsKeys key, F&& f, Args&&... args) noexcept
    {
        this->eventsCallbacks.at(static_cast<std::size_t>(key)).define(std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Clear all existing events callbacks
     *
     * @warning This will clear ALL existing callbacks
     */
    void clearEventsCallbacks() noexcept;
    /**
     * @brief Clear existing callback associated to the given event
     *
     * @param event The event to delete the associated callback
     */
    void clearEventCallbackFor(ge::KeyboardKeys event) noexcept;
    /**
     * @brief Clear existing callback associated to the given events
     *
     * @param events A list of events to delete the associated callbacks
     */
    void clearEventCallbackFor(std::initializer_list<ge::KeyboardKeys> events) noexcept;

    /**
     * @brief Treat given events
     *
     * @param events The events to treat
     */
    void treatEvents(const std::vector<Event>& events) noexcept;

   private:
    /**
     * @var pressedCallbacks
     * @brief Callbacks list with associated keys
     */
    std::array<utils::Callback<void()>, static_cast<std::size_t>(ge::KeyboardKeys::MAX)> pressedCallbacks;
    /**
     * @var releasedCallbacks
     * @brief Callbacks list with associated keys
     */
    std::array<utils::Callback<void()>, static_cast<std::size_t>(ge::KeyboardKeys::MAX)> releasedCallbacks;
    /**
     * @var eventsCallbacks
     * @brief Callbacks list with associated keys
     */
    std::array<utils::Callback<void()>, static_cast<std::size_t>(ge::EventsKeys::MAX)> eventsCallbacks;
};

} // namespace ge
