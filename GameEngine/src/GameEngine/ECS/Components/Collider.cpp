#ifdef USE_COLLISION_MODULE

#include "GameEngine/ECS/Components/Collider.hpp"

int ge::ecs::component::Collider::getType() const noexcept
{
    return this->type;
}

void ge::ecs::component::Collider::clearCallback() noexcept
{
    this->callback = {};
}

void ge::ecs::component::Collider::runCallback(ecs::core::Entity const& other, Collider const& otherCollider, ge::utils::Rect<float> collidingFaces)
{
    this->callback.safeRun(other, otherCollider, std::forward<utils::Rect<float>>(collidingFaces));
}

#endif
