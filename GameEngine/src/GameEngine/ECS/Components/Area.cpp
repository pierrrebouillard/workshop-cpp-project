#ifdef USE_AREA_MODULE

#include "GameEngine/ECS/Components/Area.hpp"

#include "GameEngine/Utils/Vector.hpp"

ge::ecs::component::Area::Area(utils::Vector size, bool centered) noexcept :
    size(size), centered(centered)
{
}

ge::ecs::component::Area& ge::ecs::component::Area::setSize(utils::Vector size) noexcept
{
    this->size = size;
    return *this;
}

ge::ecs::component::Area& ge::ecs::component::Area::setCentered(bool centered) noexcept
{
    this->centered = centered;
    return *this;
}

ge::utils::Vector ge::ecs::component::Area::getSize() const noexcept
{
    return this->size;
}

bool ge::ecs::component::Area::getCentered() const noexcept
{
    return this->centered;
}

#endif
