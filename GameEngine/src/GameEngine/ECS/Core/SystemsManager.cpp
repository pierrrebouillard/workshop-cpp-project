#include "GameEngine/ECS/Core/SystemsManager.hpp"

void ge::ecs::core::SystemsManager::entitySignatureChanged(Entity const& entity, ComponentSignature const& newEntitySignature) noexcept
{
    for (const auto& system : this->systems) {
        if (newEntitySignature.contains(this->systemsSignatures[system.first])) {
            system.second->addEntity(entity);
        } else {
            system.second->removeEntity(entity);
        }
    }
}

void ge::ecs::core::SystemsManager::entityDestroyed(Entity const& entity) noexcept
{
    for (const auto& system : this->systems) {
        system.second->removeEntity(entity);
    }
}
