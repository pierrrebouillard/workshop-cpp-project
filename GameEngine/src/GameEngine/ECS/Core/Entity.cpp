#include "GameEngine/ECS/Core/Entity.hpp"

#include "GameEngine/ECS/Core/Coordinator.hpp"
#include "fmt/core.h"

ge::ecs::core::Entity::Entity(std::size_t id) noexcept :
    id(id)
{
}

ge::ecs::core::Entity::operator std::size_t() const
{
    return this->id;
}

void ge::ecs::core::Entity::swap(Entity& other) noexcept
{
    auto buf = this->id;
    this->id = other.id;
    other.id = buf;
}
