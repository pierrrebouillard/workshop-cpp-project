#ifdef USE_ANIMATION_MODULE

#include "GameEngine/ECS/Systems/AnimationSystem.hpp"

#include "GameEngine/ECS/Components/Animation.hpp"
#include "GameEngine/ECS/Components/RenderableImage.hpp"
#include "GameEngine/ECS/Core/Entity.hpp"

ge::ecs::system::AnimationSystem::AnimationSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::AnimationSystem::update(float elaspedTime)
{
    for (auto const& entity : this->entities) {
        auto& image = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);
        auto& animation = this->coordinator.getComponent<ecs::component::Animation>(entity);

        if (animation.checkNextFrame(elaspedTime)) {
            image.setArea(animation.getNewArea());
        }
    }
}

#endif