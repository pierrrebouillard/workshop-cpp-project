#ifdef USE_IMAGE_MODULE

#include "GameEngine/ECS/Systems/ParallaxSystem.hpp"

#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/Components/RenderableImage.hpp"
#include "SFML/Graphics/Rect.hpp"

ge::ecs::system::ParallaxSystem::ParallaxSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::ParallaxSystem::update()
{
    for (auto const& entity : this->entities) {
        auto& image = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);
        auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entity);

        sf::FloatRect size = image.getBounds();
        if (attributes.position.x < size.width / -2) {
            attributes.position.x += size.width / 2;
        }
    }
}

#endif