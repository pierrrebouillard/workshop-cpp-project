#if defined USE_AREA_MODULE && defined USE_COLLISION_MODULE

#include "GameEngine/ECS/Systems/AreaBoundsSystem.hpp"

#include "GameEngine/ECS/Components/Area.hpp"
#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/Components/Bounds.hpp"

ge::ecs::system::AreaBoundsSystem::AreaBoundsSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::AreaBoundsSystem::update()
{
    for (auto const& entity : this->entities) {
        auto& area = this->coordinator.getComponent<ecs::component::Area>(entity);
        auto& attr = this->coordinator.getComponent<ecs::component::Attributes>(entity);
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(entity);

        auto size = area.getSize();
        auto position = attr.position;
        if (area.getCentered()) {
            position += {size.x / 2, size.y / 2};
        }
        bounds.bounds = {position, size};
    }
}

#endif