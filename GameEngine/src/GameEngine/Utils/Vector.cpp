#include "GameEngine/Utils/Vector.hpp"

ge::utils::Vector::operator sf::Vector2f() const
{
    return {this->x, this->y};
}

ge::utils::Vector::operator sf::Vector2i() const
{
    return {static_cast<int>(this->x), static_cast<int>(this->y)};
}

std::size_t ge::utils::VectorHash::operator()(const Vector& vector) const noexcept
{
    std::size_t h1 = std::hash<float>()(vector.x);
    std::size_t h2 = std::hash<float>()(vector.y);

    return (h1 ^ h2);
}
