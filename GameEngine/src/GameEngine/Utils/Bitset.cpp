#include "GameEngine/Utils/Bitset.hpp"

#include <bitset>
#include <utility>

void ge::utils::Bitset::clear() noexcept
{
    this->data.clear();
}

bool ge::utils::Bitset::operator!=(const Bitset& other) const
{
    const auto& longer = this->data.size() > other.data.size() ? this->data : other.data;
    const auto& shorter = this->data.size() > other.data.size() ? other.data : this->data;

    for (int i = shorter.size(); i < longer.size(); i++) {
        if (longer[i] != 0) {
            return true;
        }
    }

    for (int i = 0; i < shorter.size(); i++) {
        if ((this->data[i] ^ other.data[i]) != 0) {
            return true;
        }
    }

    return false;
}

bool ge::utils::Bitset::operator==(const Bitset& other) const
{
    return !(*this != other);
}

void ge::utils::Bitset::set(std::size_t i, bool value) noexcept
{
    auto const byte = i / 8;
    auto const bit = i % 8;

    if (byte >= this->data.size()) {
        this->data.resize(byte + 1);
    }
    if (value) {
        this->data[byte] |= (1 << bit);
    } else {
        this->data[byte] &= ~(1 << bit);
    }
}

bool ge::utils::Bitset::get(std::size_t i) const noexcept
{
    auto const byte = i / 8;
    auto const bit = i % 8;

    if (byte >= this->data.size()) {
        return false;
    }

    return (this->data[byte] & (1 << bit)) > 0;
}

bool ge::utils::Bitset::contains(const Bitset& other) const noexcept
{
    if (other.data.size() > this->data.size()) {
        for (int i = this->data.size(); i < other.data.size(); i++) {
            if (other.data[i] != 0) {
                return false;
            }
        }
    }

    const auto shorterSize = this->data.size() > other.data.size() ? other.data.size() : this->data.size();

    for (int i = 0; i < shorterSize; i++) {
        if ((this->data[i] & other.data[i]) != other.data[i]) {
            return false;
        }
    }
    return true;
}

std::string ge::utils::Bitset::toString() const noexcept
{
    std::string str;
    for (const Byte& byte : this->data) {
        std::bitset<8> bs(byte);
        str = std::string{bs.to_string()}.append(str);
    }

    return str;
}

std::vector<ge::utils::Byte> ge::utils::Bitset::extractData() const noexcept
{
    return this->data;
}

void ge::utils::Bitset::importData(std::vector<Byte> binaryData) noexcept
{
    this->data = std::move(binaryData);
}

// NOLINTNEXTLINE
std::ostream& operator<<(std::ostream& out, ge::utils::Bitset bitset)
{
    out << bitset.toString();
    auto const data = bitset.extractData();
    for (const ge::utils::Byte& byte : data) {
        std::bitset<8> bs(byte);
        out << bs.to_string() << " " << std::endl;
    }

    return out;
}
