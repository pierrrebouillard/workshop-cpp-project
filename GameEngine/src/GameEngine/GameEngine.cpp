#include "GameEngine/GameEngine.hpp"

#include "GameEngine/ECS/Components/Animation.hpp"
#include "GameEngine/ECS/Components/Area.hpp"
#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/Components/Collider.hpp"
#include "GameEngine/ECS/Components/Parallax.hpp"
#include "GameEngine/ECS/Components/RenderableImage.hpp"
#include "GameEngine/ECS/Components/RenderableText.hpp"
#include "GameEngine/ECS/Components/Transform.hpp"
#include "GameEngine/ECS/Systems/AnimationSystem.hpp"
#include "GameEngine/ECS/Systems/AreaBoundsSystem.hpp"
#include "GameEngine/ECS/Systems/BoundsPreviewSystem.hpp"
#include "GameEngine/ECS/Systems/CollisionSystem.hpp"
#include "GameEngine/ECS/Systems/ImageBoundsSystem.hpp"
#include "GameEngine/ECS/Systems/ImageRenderSystem.hpp"
#include "GameEngine/ECS/Systems/ParallaxSystem.hpp"
#include "GameEngine/ECS/Systems/TextBoundsSystem.hpp"
#include "GameEngine/ECS/Systems/TextRenderSystem.hpp"
#include "GameEngine/ECS/Systems/TransformSystem.hpp"

ge::ecs::core::Coordinator& ge::GameEngine::getCoordinator() noexcept
{
    return getInstance().coordinator;
}

ge::Window& ge::GameEngine::getWindow() noexcept
{
    return getInstance().window;
}

ge::EventsHandler& ge::GameEngine::getEventsHandler() noexcept
{
    return getInstance().eventsHandler;
}

void ge::GameEngine::start()
{
    auto& instance = getInstance();

    auto& transformSystem = ge::getCoordinator().registerSystem<ecs::system::TransformSystem, ge::ecs::component::Attributes, ge::ecs::component::Transform>();
#ifdef USE_ANIMATION_MODULE
    auto& animationSystem = ge::getCoordinator().registerSystem<ecs::system::AnimationSystem, ge::ecs::component::Animation, ge::ecs::component::RenderableImage>();
#endif
#ifdef USE_IMAGE_MODULE
    auto& imageRenderSystem = ge::getCoordinator().registerSystem<ecs::system::ImageRenderSystem, ge::ecs::component::Attributes, ge::ecs::component::RenderableImage>();
#endif
#ifdef USE_TEXT_MODULE
    auto& textRenderSystem = ge::getCoordinator().registerSystem<ecs::system::TextRenderSystem, ge::ecs::component::Attributes, ge::ecs::component::RenderableText>();
#endif
#if defined USE_IMAGE_MODULE && defined USE_COLLISION_MODULE
    auto& imageBoundsSystem = ge::getCoordinator().registerSystem<ecs::system::ImageBoundsSystem, ge::ecs::component::RenderableImage, ge::ecs::component::Bounds>();
#endif
#if defined USE_TEXT_MODULE && defined USE_COLLISION_MODULE
    auto& textBoundsSystem = ge::getCoordinator().registerSystem<ecs::system::TextBoundsSystem, ge::ecs::component::RenderableText, ge::ecs::component::Bounds>();
#endif
#if defined USE_AREA_MODULE && defined USE_COLLISION_MODULE
    auto& areaBoundsSystem = ge::getCoordinator().registerSystem<ecs::system::AreaBoundsSystem, ge::ecs::component::Area, ge::ecs::component::Attributes, ge::ecs::component::Bounds>();
#endif
#ifdef USE_COLLISION_MODULE
    auto& boundsPreviewSystem = ge::getCoordinator().registerSystem<ecs::system::BoundsPreviewSystem, ge::ecs::component::Transform, ge::ecs::component::Bounds>();
#endif
#ifdef USE_COLLISION_MODULE
    auto& collisionSystem = ge::getCoordinator().registerSystem<ecs::system::CollisionSystem, ge::ecs::component::Bounds, ge::ecs::component::Collider>();
#endif
#ifdef USE_IMAGE_MODULE
    auto& parallaxSystem = ge::getCoordinator().registerSystem<ecs::system::ParallaxSystem, ge::ecs::component::RenderableImage, ge::ecs::component::Attributes, ge::ecs::component::Parallax>();
#endif

    sf::Clock clock;
    float dt = 0;
    while (instance.window.isOpen()) {
        dt = clock.getElapsedTime().asMilliseconds();
        clock.restart();

        if (instance.inSlow) {
            dt /= instance.slowingValue;
        }
        if (instance.fpsDisplayEnabled) {
            std::cout << "FPS: " << float{1000} / (instance.inSlow ? dt * instance.slowingValue : dt) << std::endl;
        }

        if (instance.scene != nullptr) {
            instance.scene->update(float{dt});
        }

        instance.window.clear();
#ifdef USE_IMAGE_MODULE
        imageRenderSystem.update(ge::getWindow());
#endif
#ifdef USE_TEXT_MODULE
        textRenderSystem.update(ge::getWindow());
#endif
        instance.window.display();

        if (not instance.inPause) {
#ifdef USE_ANIMATION_MODULE
            animationSystem.update(dt);
#endif
#if defined USE_IMAGE_MODULE && defined USE_COLLISION_MODULE
            imageBoundsSystem.update();
#endif
#if defined USE_TEXT_MODULE && defined USE_COLLISION_MODULE
            textBoundsSystem.update();
#endif
#if defined USE_AREA_MODULE && defined USE_COLLISION_MODULE
            areaBoundsSystem.update();
#endif
#ifdef USE_COLLISION_MODULE
            boundsPreviewSystem.update(dt);
#endif
#ifdef USE_COLLISION_MODULE
            collisionSystem.update();
#endif
            transformSystem.update(dt);
#ifdef USE_IMAGE_MODULE
            parallaxSystem.update();
#endif
        }
        instance.eventsHandler.treatEvents(instance.window.getEvents());
    }
    instance.scene.reset(nullptr);
}

void ge::GameEngine::enableGamePause(KeyboardKeys key) noexcept
{
    auto& instance = getInstance();

    disableGamePause();
    instance.pauseEnabled = true;
    instance.pauseKey = key;
    getEventsHandler().setPressedKeyCallback(key, &GameEngine::handlePause, std::ref(instance));
}

void ge::GameEngine::disableGamePause() noexcept
{
    auto& instance = getInstance();

    if (instance.pauseEnabled) {
        getEventsHandler().clearEventCallbackFor(instance.pauseKey);
    }
    instance.pauseEnabled = false;
    instance.pauseKey = KeyboardKeys::Undefined;
    instance.inPause = false;
}

void ge::GameEngine::enableGameSlow(KeyboardKeys key, float slowingValue) noexcept
{
    if (slowingValue < 1) {
        slowingValue = 1;
    }

    auto& instance = getInstance();

    disableGameSlow();
    instance.slowingEnabled = true;
    instance.slowingValue = slowingValue;
    instance.slowKey = key;
    getEventsHandler().setPressedKeyCallback(key, &GameEngine::handleSlow, std::ref(instance));
}

void ge::GameEngine::disableGameSlow() noexcept
{
    auto& instance = getInstance();

    if (instance.slowingEnabled) {
        getEventsHandler().clearEventCallbackFor(instance.slowKey);
    }
    instance.slowingEnabled = false;
    instance.slowKey = KeyboardKeys::Undefined;
    instance.slowingValue = 1;
    instance.inSlow = false;
}

void ge::GameEngine::showFPS(bool show) noexcept
{
    getInstance().fpsDisplayEnabled = show;
}

void ge::GameEngine::handlePause() noexcept
{
    this->inPause = not this->inPause;
}

void ge::GameEngine::handleSlow() noexcept
{
    this->inSlow = not this->inSlow;
}
