#include "NetworkError.hpp"

network::error::NetworkError::NetworkError(std::string const& message) :
    message(message)
{
}

const char* network::error::NetworkError::what() const noexcept
{
    return message.c_str();
}
