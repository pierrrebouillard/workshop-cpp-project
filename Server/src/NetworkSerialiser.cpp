#include "NetworkSerialiser.hpp"

#include "Offset.hpp"

#include <iostream>

std::vector<Byte> network::NetworkSerialiser::convertPlayerToBinary(struct playerInfo& player) noexcept
{
    std::vector<Byte> rowData;

    std::vector<Byte> offset = this->bconverter.convertUShortToBinary(PLAYER);

    std::vector<Byte> id = this->bconverter.convertUShortToBinary(player.id);
    std::vector<Byte> pos = this->bconverter.convertVector2f(player.pos);
    std::vector<Byte> life = this->bconverter.convertUShortToBinary(player.life);
    std::vector<Byte> damage = this->bconverter.convertUShortToBinary(player.damage);

    rowData.insert(rowData.end(), offset.begin(), offset.end());
    rowData.insert(rowData.end(), id.begin(), id.end());
    rowData.insert(rowData.end(), pos.begin(), pos.end());
    rowData.insert(rowData.end(), life.begin(), life.end());
    rowData.insert(rowData.end(), damage.begin(), damage.end());
    return rowData;
}

struct playerInfo network::NetworkSerialiser::convertBinaryToPlayer(std::vector<Byte>& rowData) noexcept
{
    struct playerInfo player;

    player.id = this->bconverter.convertBinaryToUShort(rowData);
    player.pos = this->bconverter.convertBinaryToVector2f(rowData);
    player.life = this->bconverter.convertBinaryToUShort(rowData);
    player.damage = this->bconverter.convertBinaryToUShort(rowData);

    return player;
}

std::vector<Byte> network::NetworkSerialiser::convertEnemyToBinary(struct enemyInfo& enemy) noexcept
{
    std::vector<Byte> rowData;

    std::vector<Byte> offset = this->bconverter.convertUShortToBinary(ENEMY);

    std::vector<Byte> pos = this->bconverter.convertVector2f(enemy.pos);
    std::vector<Byte> life = this->bconverter.convertUShortToBinary(enemy.life);
    std::vector<Byte> damage = this->bconverter.convertUShortToBinary(enemy.damage);
    std::vector<Byte> id = this->bconverter.convertUShortToBinary(enemy.id);
    std::vector<Byte> enemyTypeID = this->bconverter.convertUShortToBinary(enemy.enemyTypeID);

    rowData.insert(rowData.end(), offset.begin(), offset.end());
    rowData.insert(rowData.end(), pos.begin(), pos.end());
    rowData.insert(rowData.end(), life.begin(), life.end());
    rowData.insert(rowData.end(), damage.begin(), damage.end());
    rowData.insert(rowData.end(), id.begin(), id.end());
    rowData.insert(rowData.end(), enemyTypeID.begin(), enemyTypeID.end());
    return rowData;
}

struct enemyInfo network::NetworkSerialiser::convertBinaryToEnemy(std::vector<Byte>& rowData) noexcept
{
    struct enemyInfo enemy;

    enemy.pos = this->bconverter.convertBinaryToVector2f(rowData);
    enemy.life = this->bconverter.convertBinaryToUShort(rowData);
    enemy.damage = this->bconverter.convertBinaryToUShort(rowData);
    enemy.id = this->bconverter.convertBinaryToUShort(rowData);
    enemy.enemyTypeID = this->bconverter.convertBinaryToUShort(rowData);

    return enemy;
}

std::vector<Byte> network::NetworkSerialiser::convertProjectileToBinary(struct projectile& projectile) noexcept
{
    std::vector<Byte> rowData;

    std::vector<Byte> offset = this->bconverter.convertUShortToBinary(PROJECTILE);

    std::vector<Byte> damage = this->bconverter.convertUShortToBinary(projectile.damage);
    std::vector<Byte> pos = this->bconverter.convertVector2f(projectile.pos);
    std::vector<Byte> id = this->bconverter.convertUShortToBinary(projectile.id);
    std::vector<Byte> projectileTypeID = this->bconverter.convertUShortToBinary(projectile.projectileTypeID);
    std::vector<Byte> ownerId = this->bconverter.convertUShortToBinary(projectile.ownerId);

    rowData.insert(rowData.end(), offset.begin(), offset.end());
    rowData.insert(rowData.end(), damage.begin(), damage.end());
    rowData.insert(rowData.end(), pos.begin(), pos.end());
    rowData.insert(rowData.end(), id.begin(), id.end());
    rowData.insert(rowData.end(), projectileTypeID.begin(), projectileTypeID.end());
    rowData.insert(rowData.end(), ownerId.begin(), ownerId.end());
    return rowData;
}

struct projectile network::NetworkSerialiser::convertBinaryToProjectile(std::vector<Byte>& rowData) noexcept
{
    struct projectile projec;

    projec.damage = this->bconverter.convertBinaryToUShort(rowData);
    projec.pos = this->bconverter.convertBinaryToVector2f(rowData);
    projec.id = this->bconverter.convertBinaryToUShort(rowData);
    projec.projectileTypeID = this->bconverter.convertBinaryToUShort(rowData);
    projec.ownerId = this->bconverter.convertBinaryToUShort(rowData);

    return projec;
}

unsigned short network::NetworkSerialiser::getOffset(std::vector<Byte>& rowData) noexcept
{
    return this->bconverter.convertBinaryToUShort(rowData);
}

std::vector<Byte> network::NetworkSerialiser::addSignature(unsigned short OFFSET) noexcept
{
    return this->bconverter.convertUShortToBinary(OFFSET);
}
