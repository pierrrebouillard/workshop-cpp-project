@echo off

:: set -eo pipefail  ==>  command || exit 1

:: ======================
:: Variables Declarations
:: ======================

SET NoColor="[0m"
SET CyanColor="[36m"
SET RedColor="[31m"

SET argc=0
for %%x in (%*) do SET /A argc+=1


:: ======================
:: Display Usage
:: ======================

if %argc%==1 (
	if [%1]==[-h] CALL :display_usage %0
	if [%1]==[--help] CALL :display_usage %0
	if [%1]==[h] CALL :display_usage %0
	if [%1]==[help] CALL :display_usage %0
)


:: ======================
:: Check Dependencies
:: ======================

CALL :whichCMake
SET cmakePath=%ERRORLEVEL%
if %cmakePath%==1 (
    echo Please install cmake first
    EXIT /B 1
) else (
    echo CMake is installed
)

CALL :whichMinGW
SET MinGWPath=%ERRORLEVEL%
if %MinGWPath%==1 (
    echo Please install MinGW first
    EXIT /B 1
) else (
    echo MinGW is installed
)

CALL :whichVisualStudio
SET VisualStudioPath=%ERRORLEVEL%
if %VisualStudioPath%==1 (
    echo Please install "Visual Studio 17 2022" first
    EXIT /B 1
) else (
    echo Visual Studio is installed
)

:: ======================
:: Core of autobuild
:: ======================

if [%1]==[s] CALL :compileserver
if [%1]==[S] CALL :compileserver
if [%1]==[server] CALL :compileserver
if [%1]==[Server] CALL :compileserver

if [%1]==[c] CALL :compileclient
if [%1]==[C] CALL :compileclient
if [%1]==[client] CALL :compileclient
if [%1]==[Client] CALL :compileclient

if NOT [%1]==[s] ( if NOT [%1]==[S] ( if NOT [%1]==[server] ( if NOT [%1]==[Server] ( if NOT [%1]==[c] ( if NOT [%1]==[C] ( if NOT [%1]==[client] ( if NOT [%1]==[Client] (
	CALL :compileboth
))))))))


if [%1]==[cc] CALL :generatecc

if [%1]==[clean] (
    CALL :clean
    EXIT /B 1
) else if [%1]==[fclean] (
    CALL :fclean
    EXIT /B 1
) else if [%1]==[aclean] (
    CALL :aclean
    EXIT /B 1
) else (
    if [%1]==[re] (
        echo ------> Recompiling all the project
        CALL :fclean
    )
    if [%1]==[test] (
        echo ------> Recompiling all the project
        CALL :fclean
    )
    if [%1]==[debug] (
        echo ------> Compiling project in debug mode
        CALL :compiledebug
    ) else if [%1]==[release] (
        echo ------> Compiling project in release mode
        CALL :compilerelease
    ) else if [%1]==[test] (
        echo ------> Compiling project tests
        CALL :compiletests
    ) else (
        CALL :compiledefault
    )

    CALL :compile

    if [%1]==[test] (
        echo ------> Starting tests
        CALL :informlaunchtests
    )
)

EXIT /B 0

:: ======================
:: Functions Declarations
:: ======================

:whichCMake
	cmake --version || EXIT /B 1
EXIT /B 0

:whichMinGW
	g++ --version || EXIT /B 1
EXIT /B 0

:whichVisualStudio
    if EXIST "C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\devenv.exe" (
        EXIT /B 0
    ) else (
        EXIT /B 1
    )
EXIT /B 0


:display_usage
	echo Usage: %1 [server / client] [Options]
    echo The first argument is optional
    echo     none      Compile Server AND Client.
    echo     client    Compile only Server.
    echo     server    Compile only Client.\n
    echo Options(optional):
    echo     re        Recompile the project from scratch. Implicitly use 'fclean'.
    echo     cc        Generate a 'compile_commands' file
    echo     debug     Recompile the project in debug mode. Implicitly use 'fclean'.
    echo     release   Recompile the project in debug mode. Implicitly use 'fclean'.
    echo     test      Launch tests for the project. Implicitly use 're'.
    echo     clean     Remove all post-compilation files (binaries...). Does not compile.
    echo     fclean    Remove all post-compilation and compilation files (binaries, build dir...). Does not compile.
    echo     aclean    Remove all post-compilation and compilation files (binaries, build dir...) for all mode (client only, server only and both). Does not compile.
EXIT /B 0

:generatecc
	echo.
	echo Creating compile_commands.json file
	echo.
    SET args+=-DCMAKE_EXPORT_COMPILE_COMMANDS=ON
EXIT /B 0

:compiledefault
    SET args+=-DCMAKE_BUILD_TYPE=Default
EXIT /B 0

:compiledebug
    SET args+=-DCMAKE_BUILD_TYPE=Debug
EXIT /B 0

:compilerelease
    SET args+=-DCMAKE_BUILD_TYPE=Release
EXIT /B 0

:compiletests
    SET args+=-DTESTING=ON
EXIT /B 0

:compileserver
	echo.
	echo.
	echo /!\ Compiling only Server
	echo.
	echo.
    SET compilepath=Server
    SET builddir=.\build\server
EXIT /B 0

:compileclient
	echo.
	echo.
	echo /!\ Compiling only Client
	echo.
	echo.
    SET compilepath=Client
    SET builddir=.\build\client
EXIT /B 0

:compileboth
	echo.
	echo.
    echo /!\ Compiling both Server and Client
	echo.
	echo.
    SET compilepath=.
    SET builddir=.\build\both
EXIT /B 0

:fclean
    CALL :clean
    del sfml-*-2.dll
    RD %builddir%
EXIT /B 0

:aclean
    CALL :clean
    RD build
EXIT /B 0

:clean
    if [%compilepath%] NEQ [Server] (
        del r-type_client.exe
        del unit_tests_client
        del build\both\r-type_client.exe
        echo "======> Delete build client"
        RD build/r-type_client
        echo "======> Delete build test client"
        RD build/unit_tests_client
    )
    if [%compilepath%] NEQ [Client] (
        del r-type_server.exe
        del build\both\r-type_server.exe
        del unit_tests_server
        echo "======> Delete build server"
        RD build/r-type_server
        echo "======> Delete build test server"
        RD build/unit_tests_server
    )
EXIT /B 0


:compile
    mkdir %builddir%
    echo === cmake ===[ %cd%\%compilepath% ]===
    echo === -S    ===[ %cd% ]===
    echo === -B    ===[ %cd%\%builddir% ]===
    cmake %compilepath% -B %builddir% -G "Visual Studio 17 2022"
    cmake --build %builddir%

    COPY %builddir%\_deps\fmt-build\bin\Debug\fmtd.dll build\Debug\
    COPY %builddir%\_deps\sfml-build\bin\Debug\sfml-*.dll build\Debug\
    COPY %builddir%\_deps\sfml-src\extlibs\bin\x64\openal32.dll build\Debug\

    .\%builddir%\r-type_client.sln

EXIT /B 0

:launchtest
    if exist %1 (
        .\%1
	) else (
        echo Could not file test file '%1'
	)
EXIT /B 0

:informlaunchtests
    echo You can start the tests by executing './unit_tests_client' and './unit_tests_server' files
EXIT /B 0

:removeLinesFromFile
    @REM Enable variable update in for loop
    setlocal enableextensions enabledelayedexpansion

    @REM Create list of line to delete
    set linesToRemove=
    for /l %%i in (%1, 1, %2) do (
        set "linesToRemove=!linesToRemove! %%i"
    )

    @REM Delete lines
    (for /f "tokens=1,* delims=[]" %%a in ('type %3^|find /v /n ""') do (
        echo/%%a|findstr /x "%linesToRemove%" >nul || echo/%%b
    ))>tmp.txt

    @REM Set new file content
    DEL %3
    MOVE tmp.txt %3
EXIT /B 0