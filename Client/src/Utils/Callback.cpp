#include "Utils/Callback.hpp"

void utils::Callback::operator()() const
{
    if (this->ptr) {
        (*(this->ptr))();
    }
}
