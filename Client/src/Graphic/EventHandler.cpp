#include "Graphic/EventHandler.hpp"

#include "ECS/Components/Transform.hpp"
#include "Graphic/Window.hpp"

void graphic::EventHandler::clearPressedKeyCallbacks(sf::Keyboard::Key key) noexcept
{
    this->keyEventController.clearPressedKeyCallbacks(key);
}

void graphic::EventHandler::clearReleasedKeyCallbacks(sf::Keyboard::Key key) noexcept
{
    this->keyEventController.clearReleasedKeyCallbacks(key);
}

void graphic::EventHandler::treatKeyPressed(Window& window, const sf::Event::KeyEvent& key) noexcept
{
    switch (key.code) {
    case sf::Keyboard::Escape:
        window.close();
        break;
    default:
        break;
    }
    this->keyEventController.keyPressed(key.code);
}

void graphic::EventHandler::treatKeyReleased(Window& /*window*/, const sf::Event::KeyEvent& key) noexcept
{
    switch (key.code) {
    default:
        break;
    }
    this->keyEventController.keyReleased(key.code);
}

void graphic::EventHandler::treatEvents(Window& window, sf::RenderWindow& SFMLWindow)
{
    sf::Event e{};
    while (SFMLWindow.pollEvent(e)) {
        switch (e.type) {
        case sf::Event::Closed:
            window.close();
            break;
        case sf::Event::KeyPressed:
            this->treatKeyPressed(window, e.key);
            break;
        case sf::Event::KeyReleased:
            this->treatKeyReleased(window, e.key);
            break;
        default:
            break;
        }
        if (e.type == sf::Event::Closed) {
            window.close();
        }
    }
}
