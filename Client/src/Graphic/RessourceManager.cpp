#include "Graphic/RessourceManager.hpp"

#include "Audio/AudioError.hpp"
#include "Utils/FilePaths.hpp"
#include "fmt/core.h"

graphic::error::TextureError::TextureError(std::string const& message) :
    message(message)
{
}

const char* graphic::error::TextureError::what() const noexcept
{
    return this->message.c_str();
}

graphic::RessourceManager& graphic::RessourceManager::getInstance() noexcept
{
    static RessourceManager ressourceManager;

    return ressourceManager;
}

sf::Texture& graphic::RessourceManager::getTexture(std::string filepath)
{
    filepath = fmt::format("{}/images/{}", ASSETSPATH, filepath);
    auto& instance = RessourceManager::getInstance();

    if (not instance.textures.contains(filepath)) {
        sf::Texture texture;
        if (not texture.loadFromFile(filepath)) {
            throw error::TextureError(fmt::format("Cannot use texture file: {}", filepath));
        }
        instance.textures.emplace(filepath, texture);
    }

    return instance.textures.at(filepath);
}

sf::SoundBuffer& graphic::RessourceManager::getSoundBuffer(std::string filepath)
{
    filepath = fmt::format("{}/sounds/{}", ASSETSPATH, filepath);
    auto& instance = RessourceManager::getInstance();

    if (not instance.sounds.contains(filepath)) {
        sf::SoundBuffer buffer;
        if (not buffer.loadFromFile(filepath)) {
            throw audio::error::SoundError(fmt::format("Cannot use sound file: {}", filepath));
        }
        instance.sounds.emplace(filepath, buffer);
    }

    return instance.sounds.at(filepath);
}
