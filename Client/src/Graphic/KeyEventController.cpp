#include "Graphic/KeyEventController.hpp"

void graphic::KeyEventController::clearPressedKeyCallbacks(sf::Keyboard::Key key) noexcept
{
    this->pressedKeys.at(key).clear();
}

void graphic::KeyEventController::clearReleasedKeyCallbacks(sf::Keyboard::Key key) noexcept
{
    this->releasedKeys.at(key).clear();
}

void graphic::KeyEventController::keyPressed(sf::Keyboard::Key key)
{
    if (this->pressedKeys.contains(key)) {
        for (const auto& callback : this->pressedKeys.at(key)) {
            callback();
        }
    }
}

void graphic::KeyEventController::keyReleased(sf::Keyboard::Key key)
{
    if (this->releasedKeys.contains(key)) {
        for (const auto& callback : this->releasedKeys.at(key)) {
            callback();
        }
    }
}
