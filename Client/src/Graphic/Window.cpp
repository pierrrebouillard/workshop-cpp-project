#include "Graphic/Window.hpp"

#include "SFML/System/String.hpp"
#include "SFML/Window/VideoMode.hpp"

graphic::Window::Window(ecs::core::Coordinator& coordinator, const std::string& title) noexcept :
    window(sf::VideoMode::getDesktopMode(), sf::String(title)), renderer(coordinator)
{
}

graphic::Window::Window(ecs::core::Coordinator& coordinator, unsigned int width, unsigned int height, const std::string& title, unsigned int bitsPerPixel) noexcept :
    window(sf::VideoMode({width, height}, bitsPerPixel), sf::String(title)), renderer(coordinator)
{
}

void graphic::Window::keyRepeat(bool repeat) noexcept
{
    this->window.setKeyRepeatEnabled(repeat);
}

bool graphic::Window::isOpen() const noexcept
{
    return this->window.isOpen();
}

void graphic::Window::close() noexcept
{
    this->window.close();
}

void graphic::Window::render() noexcept
{
    this->window.clear();
    this->renderer.display(this->window);
    this->window.display();
}

void graphic::Window::treatEvents()
{
    this->eventHandler.treatEvents(*this, this->window);
}
