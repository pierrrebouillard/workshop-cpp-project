#include "Game/Enemy.hpp"

#include "ECS/Components/Collider.hpp"
#include "Game/Explosion.hpp"
#include "Utils/FilePaths.hpp"
#include "Utils/Randomizer.hpp"

game::AEnemy::AEnemy(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager) :
    coordinator(coordinator), entity(coordinator.createEntity()), explosionManager(explosionManager), bulletManager(bulletManager)
{
    coordinator.setComponent<ecs::component::Bounds>(this->entity);
    coordinator.setComponent<ecs::component::Collider>(this->entity, ecs::component::ColliderType::DAMAGER, ecs::component::ColliderType::DAMAGEABLE).addDamagerCallback(&AEnemy::hit, &(*this)).addObstacleCallback(&AEnemy::die, &(*this));
}

game::AEnemy::AEnemy(AEnemy&& other) noexcept :
    coordinator(other.coordinator), explosionManager(other.explosionManager), bulletManager(other.bulletManager), entity(std::move(other.entity)), valid(other.valid)
{
    other.valid = false;
}

game::AEnemy::~AEnemy() noexcept
{
    if (this->valid) {
        if (this->explosing) {
            this->explosionManager.createExplosion(this->coordinator, this->coordinator.getComponent<ecs::component::Attributes>(this->entity).position, {this->coordinator.getComponent<ecs::component::Transform>(this->entity).movement.x, 0});
        }
        this->coordinator.destroyEntity(this->entity);
    }
}

void game::AEnemy::setExplosing(bool explosing) noexcept
{
    this->explosing = explosing;
}

void game::AEnemy::hit() noexcept
{
    this->lifePoints--;
}

void game::AEnemy::die() noexcept
{
    this->lifePoints = 0;
}

bool game::AEnemy::isAlive() const noexcept
{
    return (this->lifePoints > 0);
}

const size_t game::AEnemy::getEntityId() const noexcept
{
    return this->entity;
}

game::EnemyWorm::EnemyWorm(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager, sf::Vector2f pos) :
    AEnemy(coordinator, explosionManager, bulletManager)
{
    coordinator.setComponent<ecs::component::RenderableImage>(this->entity, EnemyWorm::spritePath, EnemyWorm::spriteArea);
    coordinator.setComponent<ecs::component::Animation>(this->entity, EnemyWorm::animationFramesNumber, EnemyWorm::spriteArea, EnemyWorm::animationFrameDuration, true).start();
    coordinator.setComponent<ecs::component::Attributes>(this->entity, {.position = pos, .scale = {EnemyWorm::scale, EnemyWorm::scale}});
    coordinator.setComponent<ecs::component::Transform>(this->entity).movement.x = -EnemyWorm::speed;
    this->setExplosing(true);
}

void game::EnemyWorm::move(float /*elapsedTime*/)
{
}

void game::EnemyWorm::shoot(float elapsedTime)
{
    this->shotTimeBank += elapsedTime;
    if (this->shotTimeBank >= this->nextShot) {
        this->shotTimeBank -= this->nextShot;
        this->nextShot = utils::Randomizer::getRandomNumber(1000, 2000);
        auto pos = this->coordinator.getComponent<ecs::component::Attributes>(this->entity).position;
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(this->entity);
        pos.x -= (bounds.bounds.width / 2) - bounds.transform.x;
        this->bulletManager.createBullet(this->coordinator, BulletType::BALL, pos, {-1, 0});
    }
}

game::EnemyShip::EnemyShip(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager, sf::Vector2f pos) :
    AEnemy(coordinator, explosionManager, bulletManager)
{
    coordinator.setComponent<ecs::component::RenderableImage>(this->entity, EnemyShip::spritePath, EnemyShip::spriteArea);
    coordinator.setComponent<ecs::component::Animation>(this->entity, EnemyShip::animationFramesNumber, EnemyShip::spriteArea, EnemyShip::animationFrameDuration, true).start();
    coordinator.setComponent<ecs::component::Attributes>(this->entity, {.position = pos, .scale = {EnemyShip::scale, EnemyShip::scale}});
    coordinator.setComponent<ecs::component::Transform>(this->entity).movement = {-EnemyShip::speed, EnemyShip::speed};
    this->setExplosing(true);
}

void game::EnemyShip::move(float elapsedTime)
{
    auto& attr = this->coordinator.getComponent<ecs::component::Attributes>(this->entity);
    auto& transform = this->coordinator.getComponent<ecs::component::Transform>(this->entity);

    this->moveTimeBank += elapsedTime;
    if (this->moveTimeBank > EnemyShip::moveDelay) {
        this->moveTimeBank -= EnemyShip::moveDelay;
        transform.movement.y = -transform.movement.y;
    }
    if (attr.position.y < 100) {
        transform.movement.y = EnemyShip::speed;
    } else if (attr.position.y > 980) {
        transform.movement.y = -EnemyShip::speed;
    }
}

void game::EnemyShip::shoot(float elapsedTime)
{
    this->shotTimeBank += elapsedTime;
    if (this->shotTimeBank >= this->nextShot) {
        this->shotTimeBank -= this->nextShot;
        this->nextShot = utils::Randomizer::getRandomNumber(1000, 2500);
        auto pos = this->coordinator.getComponent<ecs::component::Attributes>(this->entity).position;
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(this->entity);
        pos.x -= (bounds.bounds.width / 2) - bounds.transform.x;
        this->bulletManager.createBullet(this->coordinator, BulletType::ELECTRIC, pos, {-1, 0});
    }
}

game::EnemyVortex::EnemyVortex(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager, sf::Vector2f pos) :
    AEnemy(coordinator, explosionManager, bulletManager)
{
    coordinator.setComponent<ecs::component::RenderableImage>(this->entity, EnemyVortex::spritePath, EnemyVortex::spriteArea);
    coordinator.setComponent<ecs::component::Animation>(this->entity, EnemyVortex::animationFramesNumber, EnemyVortex::spriteArea, EnemyVortex::animationFrameDuration, true).start();
    coordinator.setComponent<ecs::component::Attributes>(this->entity, {.position = pos, .scale = {EnemyVortex::scale, EnemyVortex::scale}});
    coordinator.setComponent<ecs::component::Transform>(this->entity).movement.x = -EnemyVortex::speed;
}

void game::EnemyVortex::hit() noexcept
{
}

void game::EnemyVortex::move(float elapsedTime)
{
}

void game::EnemyVortex::shoot(float elapsedTime)
{
}

game::EnemyCreator::EnemyCreator(ecs::core::Coordinator& coordinator, EnemyManager& enemyManager) noexcept :
    coordinator(coordinator), enemyManager(enemyManager)
{
}

void game::EnemyCreator::update(float elapsedTime)
{
    this->lastSpawn += elapsedTime;
    if (this->lastSpawn > 1500) {
        this->lastSpawn = 0;
        sf::Vector2f position(1500, 200 + utils::Randomizer::getRandomNumber(0, 700));
        auto type = static_cast<game::EnemyType>(utils::Randomizer::getRandomNumber(0, 2));
        this->enemyManager.createEnemy(coordinator, type, position);
    }
    this->enemyManager.updateEnemies(elapsedTime);
}

game::EnemyManager::EnemyManager(game::ExplosionManager& explosionManager, game::BulletManager& bulletManager) noexcept :
    explosionManager(explosionManager), bulletManager(bulletManager)
{
}

game::EnemyManager::~EnemyManager() noexcept
{
    this->clearEnemies();
}

void game::EnemyManager::createEnemy(ecs::core::Coordinator& coordinator, game::EnemyType type, sf::Vector2f position)
{
    switch (type) {
    case EnemyType::WORM:
        this->listEnemies.emplace(this->lastExistingEnemyId, std::make_unique<EnemyWorm>(coordinator, this->explosionManager, this->bulletManager, position));
        break;
    case EnemyType::SHIP:
        this->listEnemies.emplace(this->lastExistingEnemyId, std::make_unique<EnemyShip>(coordinator, this->explosionManager, this->bulletManager, position));
        break;
    case EnemyType::VORTEX:
        this->listEnemies.emplace(this->lastExistingEnemyId, std::make_unique<EnemyVortex>(coordinator, this->explosionManager, this->bulletManager, position));
        break;
    }
    this->lastExistingEnemyId++;
}

void game::EnemyManager::updateEnemies(float elapsedTime) noexcept
{
    for (auto it = this->listEnemies.begin(); it != this->listEnemies.end();) {
        if (not it->second->isAlive()) {
            it = this->listEnemies.erase(it);
        } else {
            it++;
        }
    }
    for (const auto& enemy : this->listEnemies) {
        enemy.second->move(elapsedTime);
        enemy.second->shoot(elapsedTime);
    }
}

void game::EnemyManager::deleteEnemy(int id) noexcept
{
    auto it = this->listEnemies.find(id);

    if (it != this->listEnemies.end()) {
        this->listEnemies.erase(it);
    }
}

void game::EnemyManager::clearEnemies(bool noExplosions) noexcept
{
    for (const auto& explosion : this->listEnemies) {
        explosion.second->setExplosing(not noExplosions);
    }
    this->listEnemies.clear();
}

const size_t game::EnemyManager::getLastEnemy() const noexcept
{
    auto& enemy = this->listEnemies.at(this->lastExistingEnemyId - 1);

    if (enemy) {
        return enemy->getEntityId();
    }
    return 0;
}

bool game::EnemyManager::enemyIsAlive(const size_t& entityId) const noexcept
{
    for (const auto& enemy : this->listEnemies) {
        if (enemy.second->getEntityId() == entityId) {
            return enemy.second->isAlive();
        }
    }
    return false;
}
