#include "Game/UpdateDataFromNetwork.hpp"

#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "Game/Bullet.hpp"
#include "Game/Player.hpp"

#include <mutex>

game::UpdateDataFromNetwork::UpdateDataFromNetwork(ecs::core::Coordinator& coordinator, network::ClientNetwork& client, game::BulletManager& bulletManager, game::EnemyManager& enemyManager) :
    coordinator(coordinator), client(client), bulletManager(bulletManager), enemyManager(enemyManager)
{
}

void game::UpdateDataFromNetwork::setPlayerEntityId(const size_t entityId) noexcept
{
    this->playerEntityIds.emplace(this->client.getPlayerId(), entityId);
}

void game::UpdateDataFromNetwork::setEnemyEntityId(unsigned short id, const size_t entityId) noexcept
{
    this->enemyEntityIds.emplace(id, entityId);
}

void game::UpdateDataFromNetwork::setProjectileEntityId(unsigned short id, const size_t entityId) noexcept
{
    this->projectileEntityIds.emplace(id, entityId);
}

short game::UpdateDataFromNetwork::getProjectileId(const size_t& entityId) const noexcept
{
    for (auto it = this->projectileEntityIds.begin(); it != this->projectileEntityIds.end(); ++it) {
        if (it->second == entityId) {
            return it->first;
        }
    }
    return -1;
}

const std::unordered_map<unsigned short, size_t>& game::UpdateDataFromNetwork::getProjectileEntityIds() const noexcept
{
    return this->projectileEntityIds;
}

void game::UpdateDataFromNetwork::update()
{
    const auto players = client.getPlayersInfo();
    const auto enemies = client.getEnemiesInfo();
    const auto projectiles = client.getProjectilesInfo();

    for (auto& player : players) {
        if (player.id >= 4) {
            continue;
        }
        if (this->client.getPlayerId() == player.id) {
            auto entityId = static_cast<ecs::core::Entity>(this->playerEntityIds.at(player.id));
            auto attributes = this->coordinator.getComponent<ecs::component::Attributes>(entityId);
            this->client.updatePlayer(player.id, attributes.position, 100);
        } else if (this->playerEntityIds.find(player.id) == this->playerEntityIds.end()) {
            auto entityId = this->coordinator.createEntity();
            this->playerEntityIds.emplace(player.id, entityId);
            this->coordinator.setComponent<ecs::component::RenderableImage>(entityId, UpdateDataFromNetwork::pathPlayer, UpdateDataFromNetwork::areaPlayer);
            this->coordinator.setComponent<ecs::component::Attributes>(entityId, {.position = player.pos, .scale = {UpdateDataFromNetwork::scalePlayer, UpdateDataFromNetwork::scalePlayer}});
        } else {
            auto entityId = static_cast<ecs::core::Entity>(this->playerEntityIds.at(player.id));
            auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entityId);
            attributes.position = player.pos;
        }
    }
    for (auto& projectile : projectiles) {
        if (this->projectileEntityIds.find(projectile.id) == this->projectileEntityIds.end()) {
            auto direction = projectile.ownerId <= 3 ? sf::Vector2i{1, 0} : sf::Vector2i{0, 0};
            this->bulletManager.createBullet(this->coordinator, static_cast<game::BulletType>(projectile.projectileTypeID), projectile.pos, direction);
            auto lastBullet = this->bulletManager.getLastBullet();
            this->setProjectileEntityId(projectile.id, lastBullet);
        }
        if (projectile.ownerId == this->client.getPlayerId()) {
            auto entityId = static_cast<ecs::core::Entity>(this->projectileEntityIds.at(projectile.id));
            auto attributes = this->coordinator.getComponent<ecs::component::Attributes>(entityId);
            this->client.updateProjectile(projectile.id, attributes.position, 100, this->client.getPlayerId());
        }
    }
    for (auto& enemy : enemies) {
        if (this->enemyEntityIds.find(enemy.id) == this->enemyEntityIds.end()) {
            this->enemyManager.createEnemy(this->coordinator, static_cast<game::EnemyType>(enemy.enemyTypeID), enemy.pos);
            auto lastEnemy = this->enemyManager.getLastEnemy();
            this->setEnemyEntityId(enemy.id, lastEnemy);
        }

        if (!this->enemyManager.enemyIsAlive(this->enemyEntityIds.at(enemy.id))) {
            auto entityId = static_cast<ecs::core::Entity>(this->enemyEntityIds.at(enemy.id));
            auto attributes = this->coordinator.getComponent<ecs::component::Attributes>(entityId);
            this->client.updateEnemy(enemy.id, attributes.position, 0, 10);
        }
    }
}
