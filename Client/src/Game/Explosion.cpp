#include "Game/Explosion.hpp"

#include "Audio/SoundPlayer.hpp"
#include "Utils/FilePaths.hpp"
#include "fmt/core.h"

void game::ExplosionManager::deleteExplosion(int id) noexcept
{
    auto it = this->listExplosions.find(id);

    if (it != this->listExplosions.end()) {
        this->listExplosions.erase(it);
    }
}

void game::ExplosionManager::createExplosion(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f movement) noexcept
{
    this->listExplosions.emplace(this->lastExistingExplosionId, game::Explosion(coordinator, position, movement, utils::Callback{&game::ExplosionManager::deleteExplosion, &(*this), int{this->lastExistingExplosionId}}));
    this->lastExistingExplosionId++;
}

void game::ExplosionManager::clearExplosions() noexcept
{
    this->listExplosions.clear();
}

game::Explosion::Explosion(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f movement, utils::Callback&& callBack) :
    coordinator(coordinator), entity(coordinator.createEntity())
{
    coordinator.setComponent<ecs::component::RenderableImage>(this->entity, Explosion::spritePath, Explosion::spriteArea);
    coordinator.setComponent<ecs::component::Attributes>(this->entity, {.position = position, .scale = {Explosion::scale, Explosion::scale}});
    coordinator.setComponent<ecs::component::Animation>(this->entity, Explosion::animationFramesNumber, Explosion::spriteArea, Explosion::animationFrameDuration, false, 1, std::move(callBack)).start();
    coordinator.setComponent<ecs::component::Transform>(this->entity).movement = movement;

    audio::SoundPlayer::playSound(std::string(Explosion::soundPath));
}

game::Explosion::Explosion(Explosion&& other) noexcept :
    coordinator(other.coordinator), entity(std::move(other.entity)), valid(other.valid)
{
    other.valid = false;
}

game::Explosion::~Explosion() noexcept
{
    if (this->valid) {
        this->coordinator.destroyEntity(this->entity);
    }
}