#include "Game/Player.hpp"

#include "Game/Bullet.hpp"

#include <random>

game::Player::Player(ecs::core::Coordinator& coordinator, game::BulletManager& bulletManager, graphic::Window& window, sf::Vector2f pos, network::ClientNetwork& client) :
    coordinator(coordinator), player(coordinator.createEntity()), bulletManager(bulletManager), client(client)
{
    coordinator.setComponent<ecs::component::RenderableImage>(player, Player::pathUp, Player::spriteArea);
    coordinator.setComponent<ecs::component::Bounds>(player);
    coordinator.setComponent<ecs::component::Animation>(player, Player::animationFramesNumber, Player::spriteArea, Player::animationFrameDuration);
    coordinator.setComponent<ecs::component::Attributes>(player, {.position = pos, .scale = {Player::scale, Player::scale}});
    coordinator.setComponent<ecs::component::Transform>(player);
    coordinator.setComponent<ecs::component::Collider>(player, ecs::component::ColliderType::DAMAGEABLE).addObstacleCallback(&Player::stopMoving, &(*this), coordinator).addDamagerCallback(&graphic::Window::close, &window);

    window.setPressedKeyCallback(sf::Keyboard::Up, &Player::moveUp, &(*this), coordinator, true);
    window.setReleasedKeyCallback(sf::Keyboard::Up, &Player::moveUp, &(*this), coordinator, false);
    window.setPressedKeyCallback(sf::Keyboard::Down, &Player::moveDown, &(*this), coordinator, true);
    window.setReleasedKeyCallback(sf::Keyboard::Down, &Player::moveDown, &(*this), coordinator, false);
    window.setPressedKeyCallback(sf::Keyboard::Left, &Player::moveLeft, &(*this), coordinator, true);
    window.setReleasedKeyCallback(sf::Keyboard::Left, &Player::moveLeft, &(*this), coordinator, false);
    window.setPressedKeyCallback(sf::Keyboard::Right, &Player::moveRight, &(*this), coordinator, true);
    window.setReleasedKeyCallback(sf::Keyboard::Right, &Player::moveRight, &(*this), coordinator, false);
    window.setPressedKeyCallback(sf::Keyboard::Space, &Player::shot, &(*this), coordinator);

    this->id = client.getPlayerId();

    struct playerInfo infosPlayer {
        this->id,
            pos,
            100,
            10
    };
    client.createPlayer(infosPlayer);
}

void game::Player::stopMoving(ecs::core::Coordinator& coordinator)
{
    if (this->movingUp) {
        this->moveUp(coordinator, false);
    }
    if (this->movingDown) {
        this->moveDown(coordinator, false);
    }
    if (this->movingLeft) {
        this->moveLeft(coordinator, false);
    }
    if (this->movingRight) {
        this->moveRight(coordinator, false);
    }
}

void game::Player::moveUp(ecs::core::Coordinator& coordinator, bool moving)
{
    if (moving and not this->movingUp) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.y -= Player::speed;
        coordinator.getComponent<ecs::component::RenderableImage>(this->player).setTexture(Player::pathUp);
        coordinator.getComponent<ecs::component::Animation>(this->player).reverse(false).start();
        this->movingUp = true;
    } else if (not moving and this->movingUp) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.y += Player::speed;
        coordinator.getComponent<ecs::component::Animation>(this->player).reverse(true).start();
        this->movingUp = false;
    }
}

void game::Player::moveDown(ecs::core::Coordinator& coordinator, bool moving)
{
    if (moving and not this->movingDown) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.y += Player::speed;
        coordinator.getComponent<ecs::component::RenderableImage>(this->player).setTexture(Player::pathDown);
        coordinator.getComponent<ecs::component::Animation>(this->player).reverse(false).start();
        this->movingDown = true;
    } else if (not moving and this->movingDown) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.y -= Player::speed;
        coordinator.getComponent<ecs::component::Animation>(this->player).reverse(true).start();
        this->movingDown = false;
    }
}

void game::Player::moveLeft(ecs::core::Coordinator& coordinator, bool moving)
{
    if (moving and not this->movingLeft) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.x -= Player::speed;
        this->movingLeft = true;
    } else if (not moving and this->movingLeft) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.x += Player::speed;
        this->movingLeft = false;
    }
}

void game::Player::moveRight(ecs::core::Coordinator& coordinator, bool moving)
{
    if (moving and not this->movingRight) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.x += Player::speed;
        this->movingRight = true;
    } else if (not moving and this->movingRight) {
        coordinator.getComponent<ecs::component::Transform>(this->player).movement.x -= Player::speed;
        this->movingRight = false;
    }
}

unsigned short generateId()
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<unsigned short> dist(0, 50000);
    return dist(mt);
}

void game::Player::shot(ecs::core::Coordinator& coordinator)
{
    if (this->shotClock.getElapsedTime().asMilliseconds() > Player::shotDelay) {
        this->shotClock.restart();
        auto pos = coordinator.getComponent<ecs::component::Attributes>(this->player).position;
        auto& bounds = coordinator.getComponent<ecs::component::Bounds>(this->player);
        pos.x += (bounds.bounds.width / 2) + bounds.transform.x;

        unsigned short maxId = 0;
        for (auto& projectile : this->client.getProjectilesInfo()) {
            if (projectile.id > maxId) {
                maxId = projectile.id;
            }
        }
        maxId++;
        struct projectile infosProjectile = {10, {pos.x + 50, pos.y}, maxId, BulletType::ELECTRIC, this->id};
        this->client.createProjectile(infosProjectile);
    }
}

const size_t game::Player::getEntity() const noexcept
{
    return this->player;
}
