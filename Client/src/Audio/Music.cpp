#include "Audio/Music.hpp"

#include "Audio/AudioError.hpp"
#include "Utils/FilePaths.hpp"
#include "fmt/core.h"

audio::Music::Music(std::string filepath)
{
    filepath = fmt::format("{}/musics/{}", ASSETSPATH, filepath);
    if (not this->music.openFromFile(filepath)) {
        throw error::MusicError(fmt::format("Invalid music file: {}", filepath));
    }
}

audio::Music::~Music() noexcept
{
    this->stop();
}

void audio::Music::play() noexcept
{
    this->music.play();
}

void audio::Music::restart() noexcept
{
    this->music.stop();
    this->music.play();
}

void audio::Music::pause() noexcept
{
    this->music.pause();
}

void audio::Music::stop() noexcept
{
    this->music.stop();
}

void audio::Music::setVolume(float volume) noexcept
{
    this->music.setVolume(volume);
}

void audio::Music::setLoop(bool looping) noexcept
{
    this->music.setLoop(looping);
}
