#include "ECS/Core/Entity.hpp"

#include "ECS/Core/Coordinator.hpp"
#include "fmt/core.h"

#include <iostream>

ecs::core::Entity::Entity(std::size_t id) noexcept :
    id(id)
{
}

ecs::core::Entity::operator std::size_t() const
{
    return this->id;
}
