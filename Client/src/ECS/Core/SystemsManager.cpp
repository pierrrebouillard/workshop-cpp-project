#include "ECS/Core/SystemsManager.hpp"

void ecs::core::SystemsManager::entitySignatureChanged(Entity& entity, ComponentSignature const& newEntitySignature) noexcept
{
    for (const auto& system : this->systems) {
        if (newEntitySignature.contains(this->systemsSignatures[system.first])) {
            system.second->addEntity(entity);
        } else {
            system.second->removeEntity(entity);
        }
    }
}

void ecs::core::SystemsManager::entityDestroyed(Entity& entity) noexcept
{
    for (const auto& system : this->systems) {
        system.second->removeEntity(entity);
    }
}
