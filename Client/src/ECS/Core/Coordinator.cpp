#include "ECS/Core/Coordinator.hpp"

ecs::core::Entity ecs::core::Coordinator::createEntity() noexcept
{
    return this->entitiesManager.createEntity();
}

void ecs::core::Coordinator::destroyEntity(Entity& entity) noexcept
{
    this->systemsManager.entityDestroyed(entity);
    this->entitiesManager.destroyEntity(entity);
}
