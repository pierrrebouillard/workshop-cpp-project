#include "ECS/Systems/ImageRenderSystem.hpp"

#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/RenderableImage.hpp"

ecs::system::ImageRenderSystem::ImageRenderSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::ImageRenderSystem::update(sf::RenderWindow& window) const
{
    for (auto const& entity : this->entities) {
        auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entity);
        auto& image = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);

        image.setPosition(attributes.position);
        image.setScale(attributes.scale);
        image.setRotation(attributes.angle);

        image.draw(window);
    }
}
