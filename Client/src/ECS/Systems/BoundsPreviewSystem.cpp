#include "ECS/Systems/BoundsPreviewSystem.hpp"

#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/Transform.hpp"

ecs::system::BoundsPreviewSystem::BoundsPreviewSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::BoundsPreviewSystem::update(float elapsedTime) const
{
    for (auto const& entity : this->entities) {
        auto& transform = this->coordinator.getComponent<ecs::component::Transform>(entity);
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(entity);

        bounds.transform = (transform.movement * elapsedTime) / float{1000};
    }
}
