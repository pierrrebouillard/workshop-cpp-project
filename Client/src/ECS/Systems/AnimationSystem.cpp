#include "ECS/Systems/AnimationSystem.hpp"

#include "ECS/Components/Animation.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Core/Entity.hpp"

ecs::system::AnimationSystem::AnimationSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::AnimationSystem::update(float elaspedTime) const
{
    for (auto const& entity : this->entities) {
        auto& image = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);
        auto& animation = this->coordinator.getComponent<ecs::component::Animation>(entity);

        if (animation.checkNextFrame(elaspedTime)) {
            image.setArea(animation.getNewArea());
        }
    }
}