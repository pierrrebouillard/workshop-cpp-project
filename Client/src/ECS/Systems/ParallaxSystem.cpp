#include "ECS/Systems/ParallaxSystem.hpp"

#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "SFML/Graphics/Rect.hpp"

ecs::system::ParallaxSystem::ParallaxSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::ParallaxSystem::update() const
{
    for (auto const& entity : this->entities) {
        auto& image = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);
        auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entity);

        sf::FloatRect size = image.getBounds();
        if (attributes.position.x < size.width / -2) {
            attributes.position.x += size.width / 2;
        }
    }
}