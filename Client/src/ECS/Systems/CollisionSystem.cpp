#include "ECS/Systems/CollisionSystem.hpp"

#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/Collider.hpp"

#include <optional>
#include <unordered_map>

ecs::system::CollisionSystem::CollisionSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::CollisionSystem::update(std::size_t i, std::size_t index2, std::set<std::size_t> doneY) const
{
    std::set<std::size_t> doneI;

    bool firstLoop = true;
    for (; i < this->entities.size(); i++) {
        auto bounds1 = this->coordinator.getComponent<ecs::component::Bounds>(this->entities[i]);
        auto& collider1 = this->coordinator.getComponent<ecs::component::Collider>(this->entities[i]);
        bounds1.bounds.top += bounds1.transform.y;
        bounds1.bounds.left += bounds1.transform.x;

        if (not firstLoop) {
            doneY.clear();
        }
        for (std::size_t y = (firstLoop ? index2 : i + 1); y < this->entities.size(); y++) {
            doneY.insert(this->entities[y]);
            auto bounds2 = this->coordinator.getComponent<ecs::component::Bounds>(this->entities[y]);
            auto& collider2 = this->coordinator.getComponent<ecs::component::Collider>(this->entities[y]);
            bounds2.bounds.top += bounds2.transform.y;
            bounds2.bounds.left += bounds2.transform.x;

            if (bounds1.bounds.findIntersection(bounds2.bounds) != std::nullopt) {
                auto e1 = this->entities[i];
                auto e2 = this->entities[y];
                collider1.runCallbacksFor(collider2.getType());
                collider2.runCallbacksFor(collider1.getType());
                std::size_t nextI = -1;
                std::size_t nextY = -1;
                std::set<std::size_t> newDoneY;
                if (this->entities.size() > i and this->entities[i] == e1) {
                    nextI = i;
                } else {
                    for (std::size_t j = 0; j < this->entities.size(); j++) {
                        if (not doneI.contains(this->entities[j])) {
                            nextI = j;
                            break;
                        }
                    }
                }
                if (nextI == -1 or nextI >= this->entities.size()) {
                    return;
                }
                if (this->entities[nextI] == e1) {
                    for (std::size_t j = nextI + 1; j < this->entities.size(); j++) {
                        if (not doneY.contains(this->entities[j])) {
                            newDoneY = std::move(doneY);
                            nextY = (this->entities[j] == e2 ? j + 1 : j);
                            break;
                        }
                    }
                    if (nextY == -1) {
                        nextI++;
                        nextY = nextI + 1;
                    }
                } else {
                    nextY = nextI + 1;
                }
                if (nextY == -1 or nextY >= this->entities.size()) {
                    return;
                }
                return this->update(nextI, nextY, newDoneY);
            }
        }
        doneI.insert(this->entities[i]);
        firstLoop = false;
    }
}
