#include "ECS/Systems/AreaBoundsSystem.hpp"

#include "ECS/Components/Area.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"

ecs::system::AreaBoundsSystem::AreaBoundsSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::AreaBoundsSystem::update() const
{
    for (auto const& entity : this->entities) {
        auto& area = this->coordinator.getComponent<ecs::component::Area>(entity);
        auto& attr = this->coordinator.getComponent<ecs::component::Attributes>(entity);
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(entity);

        auto size = area.getSize();
        auto position = attr.position;
        if (area.getCentered()) {
            position += {size.x / 2, size.y / 2};
        }
        bounds.bounds = {position, size};
    }
}