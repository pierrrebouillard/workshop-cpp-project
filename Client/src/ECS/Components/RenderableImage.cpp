#include "ECS/Components/RenderableImage.hpp"

#include "ECS/Components/Attributes.hpp"
#include "ECS/ECSErrors.hpp"
#include "Graphic/RessourceManager.hpp"
#include "fmt/core.h"

ecs::component::RenderableImage::RenderableImage(std::string_view texturePath, sf::IntRect area, bool centered)
{
    this->setTexture(texturePath, area, centered);
}

ecs::component::RenderableImage::RenderableImage(std::string_view texturePath, sf::Vector2i area, bool centered) :
    RenderableImage(texturePath, sf::IntRect({0, 0}, {area.x, area.y}), centered)
{
}

ecs::component::RenderableImage::RenderableImage(RenderableImage&& other) noexcept :
    sprite(std::move(other.sprite))
{
}

ecs::component::RenderableImage& ecs::component::RenderableImage::setTexture(std::string_view texturePath, sf::IntRect area, bool centered)
{
    try {
        this->sprite.setTexture(graphic::RessourceManager::getTexture(std::string(texturePath)));
        if (area.width > 0 or area.height > 0) {
            this->sprite.setTextureRect(area);
        }
        if (centered) {
            auto size = this->sprite.getLocalBounds();
            this->sprite.setOrigin({size.width / 2, size.height / 2});
        } else {
            this->sprite.setOrigin({0, 0});
        }
    } catch (graphic::error::TextureError const& e) {
        throw error::ComponentError("RenderableImage", fmt::format("Failed to create graphic object: '{}'", e.what()));
    }

    return *this;
}

ecs::component::RenderableImage& ecs::component::RenderableImage::setTexture(std::string_view texturePath, sf::Vector2i area, bool centered)
{
    this->setTexture(texturePath, {{0, 0}, area}, centered);

    return *this;
}

ecs::component::RenderableImage& ecs::component::RenderableImage::setPosition(sf::Vector2f position) noexcept
{
    this->sprite.setPosition(position);

    return *this;
}

ecs::component::RenderableImage& ecs::component::RenderableImage::setScale(sf::Vector2f scale) noexcept
{
    this->sprite.setScale(scale);

    return *this;
}

ecs::component::RenderableImage& ecs::component::RenderableImage::setRotation(float angle) noexcept
{
    this->sprite.setRotation(sf::degrees(angle));

    return *this;
}

ecs::component::RenderableImage& ecs::component::RenderableImage::draw(sf::RenderWindow& window) noexcept
{
    window.draw(this->sprite);

    return *this;
}

ecs::component::RenderableImage& ecs::component::RenderableImage::setArea(sf::IntRect newArea) noexcept
{
    this->sprite.setTextureRect(newArea);

    return *this;
}

sf::FloatRect ecs::component::RenderableImage::getBounds() const noexcept
{
    return this->sprite.getGlobalBounds();
}
