#include "ECS/Components/Animation.hpp"

#include "ECS/ECSErrors.hpp"
#include "fmt/core.h"

ecs::component::Animation::Animation(unsigned int frameNumber, sf::Vector2i frameSize, float frameDuration, bool loop, unsigned int startingFrame, std::optional<utils::Callback> callback) :
    frameNumber(frameNumber), currentFrame(startingFrame - 1), frameDuration(frameDuration), loop(loop), frameSize(frameSize), callback(std::move(callback))
{
    if (frameNumber < 1) {
        throw error::ComponentError("Animation", fmt::format("Invalid frame number: {}, should be greater than 0", frameNumber));
    }
    if (frameSize.x < 1 or frameSize.y < 1) {
        throw error::ComponentError("Animation", fmt::format("Invalid frame size: [{}, {}], should be greater than 0", frameSize.x, frameSize.y));
    }
    if (frameDuration < 1) {
        throw error::ComponentError("Animation", fmt::format("Invalid frame duration: {}ms, should be greater than 0", frameDuration));
    }
    if (startingFrame < 1 or startingFrame > frameNumber) {
        throw error::ComponentError("Animation", fmt::format("Invalid starting frame: {}, should be greater than 0 and lower or equal to {}", frameDuration, frameNumber));
    }
}

ecs::component::Animation& ecs::component::Animation::looping(bool looping) noexcept
{
    this->loop = looping;
    if (looping) {
        this->currentFrame = 0;
        this->currentFrameDuration = 0;
        this->upToDate = false;
    }

    return *this;
}

ecs::component::Animation& ecs::component::Animation::reverse(bool reverse) noexcept
{
    this->reversing = reverse;
    return *this;
}

ecs::component::Animation& ecs::component::Animation::goToEnd() noexcept
{
    this->currentFrame = this->frameNumber - 1;
    return *this;
}

ecs::component::Animation& ecs::component::Animation::goToStart() noexcept
{
    this->currentFrame = 0;
    return *this;
}

ecs::component::Animation& ecs::component::Animation::setFrameDuration(float newDuration)
{
    if (newDuration < 1) {
        throw error::ComponentError("Animation", fmt::format("Invalid frame duration: {}ms, should be greater than 0", newDuration));
    }
    this->frameDuration = newDuration;

    return *this;
}

ecs::component::Animation& ecs::component::Animation::setFrameNumber(unsigned int newFrameNumber)
{
    if (newFrameNumber < 1) {
        throw error::ComponentError("Animation", fmt::format("Invalid frame number: {}, should be greater than 0", newFrameNumber));
    }
    this->frameNumber = newFrameNumber;

    return *this;
}

ecs::component::Animation& ecs::component::Animation::setFrameSize(sf::Vector2i newFrameSize)
{
    if (newFrameSize.x < 1 or newFrameSize.y < 1) {
        throw error::ComponentError("Animation", fmt::format("Invalid frame size: [{}, {}], should be greater than 0", newFrameSize.x, newFrameSize.y));
    }
    this->frameSize = newFrameSize;

    return *this;
}

ecs::component::Animation& ecs::component::Animation::start() noexcept
{
    this->running = true;
    // this->currentFrame = 0;
    this->currentFrameDuration = 0;
    this->upToDate = false;

    return *this;
}

ecs::component::Animation& ecs::component::Animation::stop()
{
    this->running = false;
    if (this->callback.has_value()) {
        this->callback.value()();
    }

    return *this;
}

bool ecs::component::Animation::checkNextFrame(float elapsedTime)
{
    if (not this->running) {
        return false;
    }
    this->currentFrameDuration += elapsedTime;
    while (this->frameDuration > 0 and this->currentFrameDuration >= this->frameDuration) {
        this->currentFrameDuration -= this->frameDuration;
        if (this->reversing) {
            this->currentFrame -= 1;
        } else {
            this->currentFrame += 1;
        }
        if (this->currentFrame >= this->frameNumber or this->currentFrame < 0) {
            if (this->loop) {
                this->currentFrame = (this->reversing ? this->frameNumber - 1 : 0);
            } else {
                this->currentFrame = (this->currentFrame < 0 ? 0 : this->frameNumber - 1);
                this->running = false;
                if (this->callback.has_value()) {
                    this->callback.value()();
                }
            }
        }
        this->upToDate = false;
    }
    return (not this->upToDate);
}

sf::IntRect ecs::component::Animation::getNewArea() const noexcept
{
    return sf::IntRect{{this->frameSize.x * this->currentFrame, 0}, {this->frameSize.x, this->frameSize.y}};
}

bool ecs::component::Animation::isEnded() const noexcept
{
    return not this->running;
}

ecs::component::Animation& ecs::component::Animation::setCallback(utils::Callback&& callback) noexcept
{
    this->callback = std::move(callback);

    return *this;
}

ecs::component::Animation& ecs::component::Animation::removeCallback() noexcept
{
    this->callback = std::nullopt;

    return *this;
}
