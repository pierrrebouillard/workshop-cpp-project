#include "Scenes/EndScene.hpp"

#include "GameEngine/GameEngine.hpp"
#include "GameEngine/Utils/Vector.hpp"
#include "Scenes/GameScene.hpp"

scene::LoseScene::LoseScene(int score) noexcept :
    text(ge::getCoordinator().createEntity()), scoreText(ge::getCoordinator().createEntity()), hintText(ge::getCoordinator().createEntity()), background(ge::getCoordinator().createEntity())
{
    ge::getCoordinator().setComponent<ge::ecs::component::RenderableImage>(this->background, scene::LoseScene::backgroundPath, ge::utils::Vector{-1, -1}, false);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->background);

    ge::getCoordinator().setComponent<ge::ecs::component::RenderableText>(this->text, "GAME ENDED", true, 200);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->text, {.position = {ge::getWindow().getSize().x / 2, ge::getWindow().getSize().y * 0.2}});

    ge::getCoordinator().setComponent<ge::ecs::component::RenderableText>(this->scoreText, std::move(std::string("Your score is: ").append(std::to_string(score))), true, 100);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->scoreText, {.position = {ge::getWindow().getSize().x / 2, ge::getWindow().getSize().y * 0.6}});

    ge::getCoordinator().setComponent<ge::ecs::component::RenderableText>(this->hintText, "Press Enter to restart the game ;)\nAnd if we are annoying you, you can press Escape to quit :(", true, 30);
    ge::getCoordinator().setComponent<ge::ecs::component::Attributes>(this->hintText, {.position = {ge::getWindow().getSize().x / 2, ge::getWindow().getSize().y * 0.9}});

    ge::getEventsHandler().setPressedKeyCallback(ge::KeyboardKeys::Enter, &ge::GameEngine::setScene<scene::GameScene>);
}

scene::LoseScene::~LoseScene() noexcept
{
    ge::getEventsHandler().clearPressedKeyCallbackFor(ge::KeyboardKeys::Space);

    ge::getCoordinator().destroyEntity(this->text);
    ge::getCoordinator().destroyEntity(this->scoreText);
    ge::getCoordinator().destroyEntity(this->hintText);
}

void scene::LoseScene::update(float elapsedTime)
{
}