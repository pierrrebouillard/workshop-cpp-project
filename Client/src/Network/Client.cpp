#include "Network/Client.hpp"

#include "SFML/Config.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "SFML/Network/Packet.hpp"
#include "SFML/Network/Socket.hpp"

#include <cmath>
#include <optional>
#include <string>

network::Client::Client(sf::IpAddress adressIp, unsigned short sPort) :
    adressIp(adressIp), sPort(sPort)
{
    this->port = this->socket.getLocalPort();
    static_cast<void>(this->socket.bind(this->port));
}

network::Client::~Client() noexcept
{
    this->socket.unbind();
}
