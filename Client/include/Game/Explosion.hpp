/**
 * @file Explosion.hpp
 * @author Afilak
 * @brief game file for explosion of player and enemies
 * @version 1
 */

#pragma once

#include "ECS/Components/Animation.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Components/Transform.hpp"
#include "Graphic/EventHandler.hpp"
#include "Graphic/Renderer.hpp"

#include <string>
#include <unordered_map>

namespace game {

/**
 * @class Explosion
 * @brief Explosion graphic element
 */
class Explosion {
   public:
    Explosion() = delete;
    /**
     * @brief Explosion constructor
     *
     * @param coordinator The ECS coordinator
     * @param position The explosion position
     * @param movement The explosion movement
     * @param callback The function to call when the explosion is ended
     */
    Explosion(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f movement, utils::Callback&& callBack);
    Explosion(Explosion const& other) noexcept = delete;
    Explosion(Explosion&& other) noexcept;
    ~Explosion() noexcept;

    Explosion& operator=(Explosion const& other) noexcept = delete;
    Explosion& operator=(Explosion&& other) noexcept = delete;

   private:
    /**
     * @var spriteArea
     * @brief The filepath to the explosion sprite
     */
    constexpr static sf::Vector2i spriteArea{32, 30};
    /**
     * @var spritePath
     * @brief The filepath to the explosion sprite
     */
    constexpr static std::string_view spritePath = "explosion.png";
    /**
     * @var spriteSound
     * @brief The filepath to the explosion sound
     */
    constexpr static std::string_view soundPath = "explosion.wav";
    /**
     * @var animationFramesNumber
     * @brief The number of frames the animation of the explosion contains
     */
    constexpr static unsigned int animationFramesNumber = 6;
    /**
     * @var animationFrameDuration
     * @brief The time between two frame of the animation
     */
    constexpr static unsigned int animationFrameDuration = 50;
    /**
     * @var scale
     * @brief The scale of the explosion sprite
     */
    constexpr static float scale = 4;

    /**
     * @var coordinator
     * @brief The ECS coordinator
     */
    ecs::core::Coordinator& coordinator;
    /**
     * @var entity
     * @brief The ECS entity that represents the explosion
     */
    ecs::core::Entity entity;
    /**
     * @var valid
     * @brief If the element is valid or not (not valid meen has been moved in most cases)
     */
    bool valid = true;
};

/**
 * @class ExplosionManager
 * @brief Create, update and destroy explosions
 */
class ExplosionManager {
   public:
    ExplosionManager() noexcept = default;
    ExplosionManager(ExplosionManager const& other) noexcept = delete;
    ExplosionManager(ExplosionManager&& other) noexcept = delete;
    ~ExplosionManager() noexcept = default;

    ExplosionManager& operator=(ExplosionManager const& other) noexcept = delete;
    ExplosionManager& operator=(ExplosionManager&& other) noexcept = delete;

    /**
     * @brief Create a new explosion
     *
     * @param coordinator The ECS coordinator
     * @param position The position of the explosion
     * @param position The movement of the explosion
     */
    void createExplosion(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f movement) noexcept;
    /**
     * @brief Destroy all existing explosions
     */
    void clearExplosions() noexcept;

   private:
    /**
     * @brief Destroy the given explosion element
     *
     * @param id The id that represents the explosion element
     */
    void deleteExplosion(int id) noexcept;

    /**
     * @var lastExistingExplosionId
     * @brief The last explosion id
     */
    int lastExistingExplosionId = 0;
    /**
     * @var listExplosions
     * @brief The explosions elements container
     */
    std::unordered_map<int, game::Explosion> listExplosions{};
};

} // namespace game