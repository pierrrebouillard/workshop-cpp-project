/**
 * @file Enemy.hpp
 * @author afilak
 * @brief graphic enemy creation/handling
 * @version 1
 */

#pragma once

#include "Audio/SoundPlayer.hpp"
#include "ECS/Components/Animation.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Components/Transform.hpp"
#include "Game/Bullet.hpp"
#include "Game/Explosion.hpp"
#include "Graphic/EventHandler.hpp"
#include "Graphic/Renderer.hpp"
#include "Graphic/Window.hpp"
#include "Utils/Randomizer.hpp"

#include <memory>

namespace game {

/**
 * @enum EnemyType
 * @brief The different types of enemies
 */
enum EnemyType {
    WORM = 0,
    SHIP = 1,
    VORTEX = 2,
};

/**
 * @class AEnemy
 * @brief abstraction for the enemies
 */
class AEnemy {
   public:
    AEnemy() = delete;
    /**
     * @brief Enemy constructor
     *
     * @param coordinator The ECS coordinator
     * @param explosionManager The manager for creating explosion
     * @param bulletManager The manager for creating bullets
     */
    AEnemy(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager);
    AEnemy(AEnemy const& other) noexcept = delete;
    AEnemy(AEnemy&& other) noexcept;
    virtual ~AEnemy() noexcept;

    AEnemy& operator=(AEnemy const& other) noexcept = delete;
    AEnemy& operator=(AEnemy&& other) noexcept = delete;

    /**
     * @brief Set if an enemy should generate an explosion or not when it die
     *
     * @param explosing Set to `true` if the enemy should generate an explosion, `false` otherwise
     */
    void setExplosing(bool explosing) noexcept;
    /**
     * @brief Check if the enemy is alive
     *
     * @return `true` if the enemy is alive, `false` otherwise
     */
    bool isAlive() const noexcept;
    /**
     * @brief The function to call when the enemy take damages
     */
    virtual void hit() noexcept;
    /**
     * @brief The function to call to kill the enemy
     */
    virtual void die() noexcept;
    /**
     * @brief Update enemy movement
     *
     * @param elapsedTime The time passed since the last call
     */
    virtual void move(float elapsedTime) = 0;
    /**
     * @brief Update enemy shoots
     *
     * @param elapsedTime The time passed since the last call
     */
    virtual void shoot(float elapsedTime) = 0;

    const size_t getEntityId() const noexcept;

   protected:
    /**
     * @var coordinator
     * @brief The ECS coordinator
     */
    ecs::core::Coordinator& coordinator;
    /**
     * @var explosionManager
     * @brief A reference to the explosion manager
     */
    game::ExplosionManager& explosionManager;
    /**
     * @var bulletManager
     * @brief A reference to the bullet manager
     */
    game::BulletManager& bulletManager;
    /**
     * @var valid
     * @brief If the object is valid or not
     */
    bool valid = true;
    /**
     * @var explosing
     * @brief Set to `true` if an explosion should be created when the enemy is destructed
     */
    bool explosing = false;
    /**
     * @var entity
     * @brief The ECS entity that represent the enemy
     */
    ecs::core::Entity entity;
    /**
     * @var lifePoints
     * @brief The life point of the enemy
     */
    int lifePoints = 1;
};

/**
 * @class EnemyWorm
 * @brief Worm enemy
 */
class EnemyWorm final : public AEnemy {
   public:
    EnemyWorm() = delete;
    /**
     * @brief Worm enemy constructor
     *
     * @param coordinator The ECS coordinator
     * @param explosionManager The manager for creating explosion
     * @param bulletManager The manager for creating bullets
     * @param pos The enemy position
     */
    EnemyWorm(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager, sf::Vector2f pos);
    EnemyWorm(EnemyWorm const& other) noexcept = delete;
    EnemyWorm(EnemyWorm&& other) noexcept = delete;
    ~EnemyWorm() noexcept final = default;

    EnemyWorm& operator=(EnemyWorm const& other) noexcept = delete;
    EnemyWorm& operator=(EnemyWorm&& other) noexcept = delete;

    void move(float elapsedTime) final;
    void shoot(float elapsedTime) final;

   private:
    /**
     * @var speed
     * @brief The enemy speed
     */
    constexpr static float speed = 100;
    /**
     * @var spriteArea
     * @brief The size of the enemy sprite
     */
    constexpr static sf::Vector2i spriteArea{16, 9};
    /**
     * @var spritePath
     * @brief The filepath to the sprite
     */
    constexpr static std::string_view spritePath = "enemies/worm.png";
    /**
     * @var animationFramesNumber
     * @brief The number of frames the animation of the enemy contains
     */
    constexpr static unsigned int animationFramesNumber = 4;
    /**
     * @var animationFrameDuration
     * @brief The time between two frame of the animation
     */
    constexpr static unsigned int animationFrameDuration = 100;
    /**
     * @var scale
     * @brief The scale of the enemy sprite
     */
    constexpr static float scale = 5;

    /**
     * @var shotTimeBank
     * @brief The time elapsed since the last time time the ship shot a bullet
     */
    float shotTimeBank = 0;
    /**
     * @var nextShot
     * @brief The time to wait to the next time the ship should shot
     */
    float nextShot = utils::Randomizer::getRandomNumber(1000, 2000);
};

/**
 * @class EnemyShip
 * @brief Ship enemy
 */
class EnemyShip final : public AEnemy {
   public:
    EnemyShip() = delete;
    /**
     * @brief Ship enemy constructor
     *
     * @param coordinator The ECS coordinator
     * @param explosionManager The manager for creating explosion
     * @param bulletManager The manager for creating bullets
     * @param pos The enemy position
     */
    EnemyShip(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager, sf::Vector2f pos);
    EnemyShip(EnemyShip const& other) noexcept = delete;
    EnemyShip(EnemyShip&& other) noexcept = delete;
    ~EnemyShip() noexcept final = default;

    EnemyShip& operator=(EnemyShip const& other) noexcept = delete;
    EnemyShip& operator=(EnemyShip&& other) noexcept = delete;

    void move(float elapsedTime) final;
    void shoot(float elapsedTime) final;

   private:
    /**
     * @var speed
     * @brief The enemy speed
     */
    constexpr static float speed = 150;
    /**
     * @var spriteArea
     * @brief The size of the enemy sprite
     */
    constexpr static sf::Vector2i spriteArea{21, 24};
    /**
     * @var spritePath
     * @brief The filepath to the sprite
     */
    constexpr static std::string_view spritePath = "enemies/spaceShip.png";
    /**
     * @var animationFramesNumber
     * @brief The number of frames the animation of the enemy contains
     */
    constexpr static unsigned int animationFramesNumber = 8;
    /**
     * @var animationFrameDuration
     * @brief The time between two frame of the animation
     */
    constexpr static unsigned int animationFrameDuration = 100;
    /**
     * @var scale
     * @brief The scale of the enemy sprite
     */
    constexpr static float scale = 4;
    /**
     * @var moveDelay
     * @brief The minimum delay between two moves
     */
    constexpr static unsigned int moveDelay = 1000;

    /**
     * @var moveTimeBank
     * @brief The time elapsed since the last time the ship changed its direction
     */
    float moveTimeBank = 0;
    /**
     * @var shotTimeBank
     * @brief The time elapsed since the last time time the ship shot a bullet
     */
    float shotTimeBank = 0;
    /**
     * @var nextShot
     * @brief The time to wait to the next time the ship should shot
     */
    float nextShot = utils::Randomizer::getRandomNumber(1000, 4000);
};

/**
 * @class EnemyVortex
 * @brief Vortex enemy
 */
class EnemyVortex final : public AEnemy {
   public:
    EnemyVortex() = delete;
    /**
     * @brief Vortex enemy constructor
     *
     * @param coordinator The ECS coordinator
     * @param explosionManager The manager for creating explosion
     * @param bulletManager The manager for creating bullets
     * @param pos The enemy position
     */
    EnemyVortex(ecs::core::Coordinator& coordinator, game::ExplosionManager& explosionManager, game::BulletManager& bulletManager, sf::Vector2f pos);
    EnemyVortex(EnemyVortex const& other) noexcept = delete;
    EnemyVortex(EnemyVortex&& other) noexcept = delete;
    ~EnemyVortex() noexcept final = default;

    EnemyVortex& operator=(EnemyVortex const& other) noexcept = delete;
    EnemyVortex& operator=(EnemyVortex&& other) noexcept = delete;

    void move(float elapsedTime) final;
    void shoot(float elapsedTime) final;
    void hit() noexcept final;

   private:
    /**
     * @var speed
     * @brief The enemy speed
     */
    constexpr static float speed = 200;
    /**
     * @var spriteArea
     * @brief The size of the enemy sprite
     */
    constexpr static sf::Vector2i spriteArea{32, 28};
    /**
     * @var spritePath
     * @brief The filepath to the sprite
     */
    constexpr static std::string_view spritePath = "enemies/vortex.png";
    /**
     * @var animationFramesNumber
     * @brief The number of frames the animation of the enemy contains
     */
    constexpr static unsigned int animationFramesNumber = 3;
    /**
     * @var animationFrameDuration
     * @brief The time between two frame of the animation
     */
    constexpr static unsigned int animationFrameDuration = 100;
    /**
     * @var scale
     * @brief The scale of the enemy sprite
     */
    constexpr static float scale = 5;
};

/**
 * @class EnemyManager
 * @brief The class for creating, updating and destroying enemies
 */
class EnemyManager {
   public:
    EnemyManager() noexcept = delete;
    /**
     * @brief Create the enemy manager
     *
     * @param explosionManager The manager for creating explosion
     * @param bulletManager The manager for creating bullets
     */
    EnemyManager(game::ExplosionManager& explosionManager, game::BulletManager& bulletManager) noexcept;
    EnemyManager(EnemyManager const& other) noexcept = delete;
    EnemyManager(EnemyManager&& other) noexcept = delete;
    ~EnemyManager() noexcept;

    EnemyManager& operator=(EnemyManager const& other) noexcept = delete;
    EnemyManager& operator=(EnemyManager&& other) noexcept = delete;

    /**
     * @brief Update the enemies already existing
     *
     * @param elapsedTime The time elapsed since the last call
     */
    void updateEnemies(float elapsedTime) noexcept;
    /**
     * @brief Create a new enemy
     *
     * @param coordinator The ECS coordinator
     * @param type The enemy type
     * @param position The position of the enemy
     */
    void createEnemy(ecs::core::Coordinator& coordinator, EnemyType type, sf::Vector2f position);
    /**
     * @brief Destroy all existing enemies
     *
     * @param noExplosions If set to `true`, the explosions will not be created at the removed enemy position
     */
    void clearEnemies(bool noExplosions = true) noexcept;

    const size_t getLastEnemy() const noexcept;

    bool enemyIsAlive(const size_t& entityId) const noexcept;

   private:
    /**
     * @brief Delete an enemy
     *
     * @param id The id of the enemy
     */
    void deleteEnemy(int id) noexcept;

    /**
     * @var explosionManager
     * @brief A reference to the explosion manager
     */
    game::ExplosionManager& explosionManager;
    /**
     * @var bulletManager
     * @brief A reference to the bullet manager
     */
    game::BulletManager& bulletManager;
    /**
     * @var lastExistingEnemyId
     * @brief The last created enemy id
     */
    int lastExistingEnemyId = 0;
    /**
     * @var listEnemies
     * @brief The list of existing enemies
     */
    std::unordered_map<int, std::unique_ptr<game::AEnemy>> listEnemies{};
};

/**
 * @class EnemyControll
 * @brief Controll enemies creation and update
 */
class EnemyCreator {
   public:
    EnemyCreator() = delete;
    /**
     * @brief Construct an enemy manager
     *
     * @param coordinator The ECS coordinator
     * @param enemyManager The manager to create enemies
     */
    EnemyCreator(ecs::core::Coordinator& coordinator, EnemyManager& enemyManager) noexcept;
    EnemyCreator(EnemyCreator const& other) noexcept = delete;
    EnemyCreator(EnemyCreator&& other) noexcept = delete;
    ~EnemyCreator() noexcept = default;

    EnemyCreator& operator=(EnemyCreator const& other) noexcept = delete;
    EnemyCreator& operator=(EnemyCreator&& other) noexcept = delete;

    /**
     * @brief Update the enemies and create new ones if needed
     *
     * @param elapsedTime The time elapsed since the last call
     */
    void update(float elapsedTime);

   private:
    /**
     * @var coordinator
     * @brief The ECS coordinator
     */
    ecs::core::Coordinator& coordinator;
    /**
     * @var enemyManager
     * @brief Creator of enemies
     */
    game::EnemyManager& enemyManager;
    /**
     * @var lastSpawn
     * @brief The last spawn of an enemy
     */
    float lastSpawn = 0;
};

} // namespace game
