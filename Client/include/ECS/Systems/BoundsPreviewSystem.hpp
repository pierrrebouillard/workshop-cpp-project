/**
 * @file BoundsPreviewSystem.hpp
 * @brief Bounds of objects entity system definition with the preview of the transform
 * @author curs3w4ll
 * @version 1
 */

#include "ECS/Core/ASystem.hpp"
#include "ECS/Core/Coordinator.hpp"

#pragma once

namespace ecs::system {

/**
 * @class BoundsPreviewSystem
 * @brief The ECS system bounds preview with transform for objects
 */
class BoundsPreviewSystem final : public core::ASystem {
   public:
    BoundsPreviewSystem() noexcept = delete;
    explicit BoundsPreviewSystem(ecs::core::Coordinator& coordinator) noexcept;
    BoundsPreviewSystem(BoundsPreviewSystem const& other) noexcept = delete;
    BoundsPreviewSystem(BoundsPreviewSystem&& other) noexcept = default;
    ~BoundsPreviewSystem() noexcept final = default;

    BoundsPreviewSystem& operator=(BoundsPreviewSystem const& other) noexcept = delete;
    BoundsPreviewSystem& operator=(BoundsPreviewSystem&& other) noexcept = delete;

    /**
     * @brief update the bounds of the objects
     *
     * @param elapsedTime The time elapsed since the last update
     */
    void update(float elapsedTime) const;
};

} // namespace ecs::system