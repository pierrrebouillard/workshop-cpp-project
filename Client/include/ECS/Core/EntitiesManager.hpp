/**
 * @file EntitiesManager.hpp
 * @brief ECS entities managing class
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "Component.hpp"
#include "Entity.hpp"

#include <cstddef>
#include <vector>

namespace ecs::core {

/**
 * @class EntitiesManager
 * @brief Manage the entities of the ECS
 */
class EntitiesManager {
   public:
    EntitiesManager() noexcept = default;
    EntitiesManager(EntitiesManager const& other) noexcept = delete;
    EntitiesManager(EntitiesManager&& other) noexcept = default;
    ~EntitiesManager() noexcept = default;

    EntitiesManager& operator=(EntitiesManager const& other) noexcept = delete;
    EntitiesManager& operator=(EntitiesManager&& other) noexcept = default;

    /**
     * @brief Create a new ECS unique entity
     *
     * @return The newly created entity
     */
    Entity createEntity() noexcept;
    /**
     * @brief Destroy an existing entity
     *
     * @pre This entity should already have been created
     *
     * @param entity The entity to destroy
     */
    void destroyEntity(Entity& entity) noexcept;
    /**
     * @brief Set the signature of the given entity
     *
     * @pre The entity should have been created by a call to the createEntity function
     *
     * @param entity The entity to set the signature for
     * @param signature The signature to assign to the entity
     *
     * @throw error::ECSError thrown if the given entity do not exist are has already been destroyed
     */
    void setSignature(Entity& entity, ComponentSignature signature);
    /**
     * @brief Modify the signature of the given entity
     *
     * @pre The entity should have been created by a call to the createEntity function
     *
     * @param entity The entity to set the signature for
     * @param componentBit The Bit index to modify value of the signature of the entity
     * @param value The value to set the Bit to
     *
     * @throw error::ECSError thrown if the given entity do not exist are has already been destroyed
     */
    void sign(Entity& entity, Bit componentBit, bool value);
    /**
     * @brief Get the signature of an entity
     *
     * @pre The entity should have been created by a call to the createEntity function
     *
     * @param entity The entity to get signature of
     *
     * @return A copy of the signature of the entity
     *
     * @throw error::ECSError thrown if the given entity do not exist are has already been destroyed
     */
    ComponentSignature getSignature(Entity& entity) const;

   private:
    /**
     * @brief Register if the manager has already been used once
     */
    bool used = false;
    /**
     * @brief The maximum entity id created for now
     */
    std::size_t maxExistingEntity = 0;
    /**
     * @brief A list of avaible entities that have been created but then destroyed
     */
    std::vector<std::size_t> availableEntities;
    /**
     * @brief The signatures of the entities
     */
    std::vector<ComponentSignature> entitiesSignatures;
};

} // namespace ecs::core
