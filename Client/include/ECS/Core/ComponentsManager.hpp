/**
 * @file ComponentsManager.hpp
 * @brief Manage actions relative to components for the ECS
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "ComponentArray.hpp"
#include "ECS/Core/Bitset.hpp"
#include "ECS/ECSErrors.hpp"
#include "fmt/core.h"

#include <cstdint>
#include <memory>
#include <typeindex>
#include <unordered_map>

namespace ecs::core {

/**
 * @class ComponentsManager
 * @brief Manage actions on ECS components
 */
class ComponentsManager {
   public:
    ComponentsManager() noexcept = default;
    ComponentsManager(ComponentsManager const& other) noexcept = delete;
    ComponentsManager(ComponentsManager&& other) noexcept = default;
    ~ComponentsManager() noexcept = default;

    ComponentsManager& operator=(ComponentsManager const& other) noexcept = delete;
    ComponentsManager& operator=(ComponentsManager&& other) noexcept = default;

    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component
     */
    template <typename T>
    /**
     * @brief Register a new component in the ECS
     * @details A call to this function will assign the component type to a bit of a signature
     *
     * @warning This function should be called only one time for the same component
     *
     * @throw error::ECSError thrown if this function is called more than one time for the same component type
     */
    void registerComponent()
    {
        // NOLINTNEXTLINE
        auto type = std::type_index(typeid(T));

        if (this->componentsBits.contains(type)) {
            throw error::ECSError(fmt::format("Registering same component({}) two times", typeid(T).name()));
        }

        this->componentsBits.insert({type, this->nextBit++});
        this->componentsArrays.insert({type, std::make_unique<ComponentArray<T>>()});
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component
     */
    template <typename T>
    /**
     * @brief Get the Bit index associated with the given component type
     *
     * @pre The given component type should already have been registered
     *
     * @return The Bit index associated to the component type
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    Bit getComponentBit() const
    {
        // NOLINTNEXTLINE
        auto type = std::type_index(typeid(T));

        if (not this->componentsBits.contains(type)) {
            throw error::ECSError(fmt::format("Component({}) not registered before use", typeid(T).name()));
        }

        return this->componentsBits.find(type)->second;
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component
     */
    template <typename T>
    /**
     * @brief Set the given component for the given entity
     *
     * @warning The component passed to this function need to already have been constructed
     *
     * @param entity The entity to associate to the component
     * @param component The component to set for the entity
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    void setComponent(Entity& entity, T const& component)
    {
        this->getComponentArray<T>().insert_at(entity, component);
    }
    template <typename T>
    void setComponent(Entity& entity, T&& component)
    {
        this->getComponentArray<T>().insert_at(entity, std::forward<T>(component));
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component
     * @tparam Params The parameters type to pass to the component constructor
     */
    template <typename T, class... Params>
    /**
     * @brief Set and construct the given component for the given entity
     *
     * @warning The component passed to this function need to already have been constructed
     *
     * @param entity The entity to associate to the component
     * @param params The parameters that will be passed to the component constructor
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    void setComponent(Entity& entity, Params&&... params)
    {
        this->getComponentArray<T>().emplace_at(entity, std::forward<Params>(params)...);
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component to register
     */
    template <typename T>
    /**
     * @brief Destroy the component associated with the given entity
     *
     * @pre The component should have been registered and the entity should have the component in it's signature
     *
     * @param entity The entity to destroy component for
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    void eraseComponent(Entity& entity)
    {
        // NOLINTNEXTLINE
        auto type = std::type_index(typeid(T));

        if (not this->componentsBits.contains(type)) {
            throw error::ECSError(fmt::format("Component({}) not registered before use", typeid(T).name()));
        }
        this->getComponentArray<T>().erase(entity);
    }
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component
     */
    template <typename T>
    /**
     * @brief Get the component associated to the given entity
     *
     * @pre The component need to be set before calling this function
     *
     * @param entity The entity to get the component associated with
     *
     * @return A reference to the component
     *
     * @throw error::ECSError thrown if T component has not been registered
     * @throw error::ECSError thrown if the component of type T has not been setted before calling this function
     */
    T& getComponent(Entity& entity) const
    {
        auto& ret = this->getComponentArray<T>()[entity];

        if (not ret.has_value()) {
            throw error::ECSError(fmt::format("Try to get Component({}) for entity {} before setting it", typeid(T).name(), entity));
        }
        return ret.value();
    }
    template <typename T>
    T& getComponent(std::size_t entity) const
    {
        auto& ret = this->getComponentArray<T>()[entity];

        if (not ret.has_value()) {
            throw error::ECSError(fmt::format("Try to get Component({}) for entity {} before setting it", typeid(T).name(), entity));
        }
        return ret.value();
    }

   private:
    /**
     * @brief Templated component action
     *
     * @tparam T The type of the component
     */
    template <typename T>
    /**
     * @brief Get the component array for the given component type
     *
     * @pre The T component should have been registered
     *
     * @return A reference to the component array
     *
     * @throw error::ECSError thrown if T component has not been registered
     */
    ComponentArray<T>& getComponentArray() const
    {
        // NOLINTNEXTLINE
        auto type = std::type_index(typeid(T));

        if (not this->componentsBits.contains(type)) {
            throw error::ECSError(fmt::format("Component({}) not registered before use", typeid(T).name()));
        }

        return dynamic_cast<ComponentArray<T>&>(*(this->componentsArrays.at(type)));
    }

    /**
     * @brief The signature coordination of the different components
     */
    std::unordered_map<std::type_index, std::size_t> componentsBits;
    /**
     * @brief The array of the different component type arrays
     */
    std::unordered_map<std::type_index, std::unique_ptr<IComponentArray>> componentsArrays;
    /**
     * @brief The next Bit signature that is not associated with a component type
     */
    std::size_t nextBit = 0;
};

} // namespace ecs::core
