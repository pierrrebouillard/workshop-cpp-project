/**
 * @file RenderableImage.hpp
 * @brief  RenderableImage component to set a sprite on Entity
 * @author Baptiste-MV
 * @version 1
 */

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string_view>

#pragma once

namespace ecs::component {

/**
 * @class RenderableImage
 * @brief Create and store sprite and texture
 */
class RenderableImage {
   public:
    RenderableImage() noexcept = delete;
    /**
     * @brief Construct a new Renderable Image
     *
     * @param texturePath The path to the sprite file
     * @param area The texture area to render on the sprite
     * @param centered Set to `true` if the sprite origin should be centered, `false` otherwise
     */
    explicit RenderableImage(std::string_view texturePath, sf::IntRect area = {{0, 0}, {-1, -1}}, bool centered = true);
    explicit RenderableImage(std::string_view texturePath, sf::Vector2i area, bool centered = true);
    RenderableImage(RenderableImage const& other) noexcept = delete;
    RenderableImage(RenderableImage&& other) noexcept;
    ~RenderableImage() noexcept = default;

    RenderableImage& operator=(RenderableImage const& other) noexcept = delete;
    RenderableImage& operator=(RenderableImage&& other) noexcept = default;

    /**
     * @brief Set the sprite texture
     *
     * @param texturePath The path to the texture file
     * @param area The area of the texture to render on the sprite
     * @param centered Set to `true` if the sprite origin should be centered, `false` otherwise
     *
     * @return A reference to the object itself
     *
     * @throw error::ComponentError if the filepath is invalid
     */
    RenderableImage& setTexture(std::string_view texturePath, sf::IntRect area = {{0, 0}, {-1, -1}}, bool centered = true);
    RenderableImage& setTexture(std::string_view texturePath, sf::Vector2i area, bool centered = true);
    /**
     * @brief Set the sprite position
     *
     * @return A reference to the object itself
     */
    RenderableImage& setPosition(sf::Vector2f position) noexcept;
    /**
     * @brief Set the sprite scale
     *
     * @return A reference to the object itself
     */
    RenderableImage& setScale(sf::Vector2f scale) noexcept;
    /**
     * @brief Set the sprite rotation
     *
     * @return A reference to the object itself
     */
    RenderableImage& setRotation(float angle) noexcept;
    /**
     * @brief Draw the sprite on window
     *
     * @return A reference to the object itself
     */
    RenderableImage& draw(sf::RenderWindow& window) noexcept;
    /**
     * @brief Set the new sprite area
     *
     * @return A reference to the object itself
     */
    RenderableImage& setArea(sf::IntRect newArea) noexcept;
    /**
     * @brief Center the sprite
     *
     * @param centered Set to `true` if the sprite origin should be centered, `false` otherwise
     *
     * @return A reference to the object itself
     */
    RenderableImage& setCentered(bool centered = true) noexcept;
    /**
     * @brief Get the bounds of the sprite
     *
     * @return The sprite bounds
     */
    sf::FloatRect getBounds() const noexcept;

   private:
    /**
     * @var sprite
     * @brief sprite of the Entity
     */
    sf::Sprite sprite;
};

} // namespace ecs::component
