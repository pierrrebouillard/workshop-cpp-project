/**
 * @file Callback.hpp
 * @brief Function callback creator
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include <memory>
#include <tuple>

namespace utils {

/**
 * @class ACallback
 * @brief Used for internal Callback purposes
 */
class ACallback {
   public:
    ACallback() noexcept = default;
    ACallback(ACallback const& other) noexcept = default;
    ACallback(ACallback&& other) noexcept = default;
    virtual ~ACallback() noexcept = default;

    ACallback& operator=(ACallback const& other) noexcept = default;
    ACallback& operator=(ACallback&& other) noexcept = default;

    virtual void operator()() const = 0;
};

/**
 * @brief Function pointer template
 *
 * @tparam F The function definition
 * @tparam Args Arguments to pass to the function
 */
template <typename F, typename... Args>
/**
 * @class ACallback
 * @brief Used for internal Callback purposes
 */
class CallbackCaller final : public ACallback {
   public:
    CallbackCaller() noexcept = delete;
    explicit CallbackCaller(F&& f, Args&&... args) :
        f(std::forward<F>(f)), args(std::forward<Args>(args)...) { }
    CallbackCaller(CallbackCaller const& other) noexcept = delete;
    CallbackCaller(CallbackCaller&& other) noexcept = default;
    ~CallbackCaller() noexcept final = default;

    CallbackCaller& operator=(CallbackCaller const& other) noexcept = delete;
    CallbackCaller& operator=(CallbackCaller&& other) noexcept = default;

    /**
     * @brief Call the function with appropriate arguments
     *
     * @throw anything that can be thrown by the callbacks functions
     */
    void operator()() const final { std::apply(this->f, this->args); }

   private:
    /**
     * @var f
     * @brief The function that the callback will call
     */
    F f;
    /**
     * @var args
     * @brief The arguments that will be passed to the function when it will be called
     */
    std::tuple<Args...> args;
};

/**
 * @class Callback
 * @brief Create a new callback object
 */
class Callback {
   public:
    Callback() noexcept = default;
    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Create a new callback object
     *
     * @param f The function to call when the callback got triggered
     * @param args The arguments to pass to the function
     */
    explicit Callback(F&& f, Args&&... args) noexcept :
        ptr(std::make_unique<CallbackCaller<F, Args...>>(std::forward<F>(f), std::forward<Args>(args)...))
    {
    }
    Callback(Callback const& other) noexcept = delete;
    Callback(Callback&& other) noexcept = default;
    ~Callback() noexcept = default;

    Callback& operator=(Callback const& other) noexcept = delete;
    Callback& operator=(Callback&& other) noexcept = default;

    /**
     * @brief Trigerring operator
     * @details This will trigger the callback and so call the function with its arguments
     */
    void operator()() const;

   private:
    /**
     * @var ptr
     * @brief A pointer to the callable object
     */
    std::unique_ptr<ACallback> ptr = nullptr;
};

} // namespace utils
