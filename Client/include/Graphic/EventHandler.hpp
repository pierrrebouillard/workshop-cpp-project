/**
 * @file EventHandler.hpp
 * @brief Graphic window events handler
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "KeyEventController.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/Window/Event.hpp"

namespace graphic {

class Window;

/**
 * @class EventHandler
 * @brief Handle events that occurs on a graphic window
 */
class EventHandler {
   public:
    EventHandler() noexcept = default;
    EventHandler(EventHandler const& other) noexcept = delete;
    EventHandler(EventHandler&& other) noexcept = default;
    ~EventHandler() noexcept = default;

    EventHandler& operator=(EventHandler const& other) noexcept = delete;
    EventHandler& operator=(EventHandler&& other) noexcept = delete;

    /**
     * @brief Treats events that occured on the given window
     *
     * @param window The encapsulated window object
     * @param SFMLWindow The SFML graphic window
     *
     * @throw anything that can be thrown by the callbacks functions
     */
    void treatEvents(Window& window, sf::RenderWindow& SFMLWindow);
    /**
     * @brief Functions variadic template to take any function pointer with its arguments
     *
     * @tparam F The function signature
     * @tparam Args The arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback that will be triggered everytime the given key will be pressed
     *
     * @param key The key identification that will identify when the callback should be called
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setPressedKeyCallback(sf::Keyboard::Key key, F&& f, Args&&... args) noexcept
    {
        this->keyEventController.setPressedKeyCallback(key, std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Clear existing pressed callbacks associated to the given key
     *
     * @warning This will clear ALL existing callbacks
     *
     * @param key The key to delete associated callbacks
     */
    void clearPressedKeyCallbacks(sf::Keyboard::Key key) noexcept;
    /**
     * @brief Functions variadic template to take any function pointer with its arguments
     *
     * @tparam F The function signature
     * @tparam Args The arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback that will be triggered everytime the given key will be released
     *
     * @param key The key identification that will identify when the callback should be called
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setReleasedKeyCallback(sf::Keyboard::Key key, F&& f, Args&&... args) noexcept
    {
        this->keyEventController.setReleasedKeyCallback(key, std::forward<F>(f), std::forward<Args>(args)...);
    }
    /**
     * @brief Clear existing released callbacks associated to the given key
     *
     * @warning This will clear ALL existing callbacks
     *
     * @param key The key to delete associated callbacks
     */
    void clearReleasedKeyCallbacks(sf::Keyboard::Key key) noexcept;

   private:
    /**
     * @brief Treat the event when a key is pressed
     *
     * @param window The window the event occured on
     * @param key The key that is pressed
     */
    void treatKeyPressed(Window& window, const sf::Event::KeyEvent& key) noexcept;
    /**
     * @brief Treat the event when a key is released
     *
     * @param window The window the event occured on
     * @param key The key that is released
     */
    void treatKeyReleased(Window& window, const sf::Event::KeyEvent& key) noexcept;

    /**
     * @var keyEventController
     * @brief Control key input events and callbacks
     */
    KeyEventController keyEventController;
};

} // namespace graphic
