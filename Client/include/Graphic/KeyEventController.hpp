/**
 * @file KeyEventController.hpp
 * @brief Window Key actions controller
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "SFML/Window/Keyboard.hpp"
#include "Utils/Callback.hpp"

#include <unordered_map>
#include <vector>

namespace graphic {

/**
 * @class KeyEventController
 * @brief The controller and triggerer object
 */
class KeyEventController {
   public:
    KeyEventController() noexcept = default;
    KeyEventController(KeyEventController const& other) noexcept = delete;
    KeyEventController(KeyEventController&& other) noexcept = default;
    ~KeyEventController() noexcept = default;

    KeyEventController& operator=(KeyEventController const& other) noexcept = delete;
    KeyEventController& operator=(KeyEventController&& other) noexcept = default;

    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a callback function that will be called when a key get pressed
     *
     * @param key The keyboard key to associate callback to
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setPressedKeyCallback(sf::Keyboard::Key key, F&& f, Args&&... args) noexcept
    {
        if (not this->pressedKeys.contains(key)) {
            this->pressedKeys.insert({key, std::vector<utils::Callback>{}});
        }
        this->pressedKeys.at(key).push_back(utils::Callback(std::forward<F>(f), std::forward<Args>(args)...));
    }
    /**
     * @brief Clear existing pressed callbacks associated to the given key
     *
     * @warning This will clear ALL existing callbacks
     *
     * @param key The key to delete associated callbacks
     */
    void clearPressedKeyCallbacks(sf::Keyboard::Key key) noexcept;
    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a callback function that will be called when a key get pressed
     *
     * @param key The keyboard key to associate callback to
     * @param f The function to call
     * @param args The arguments to pass to the function when calling it
     */
    void setReleasedKeyCallback(sf::Keyboard::Key key, F&& f, Args&&... args) noexcept
    {
        if (not this->releasedKeys.contains(key)) {
            this->releasedKeys.insert({key, std::vector<utils::Callback>{}});
        }
        this->releasedKeys.at(key).push_back(utils::Callback(std::forward<F>(f), std::forward<Args>(args)...));
    }
    /**
     * @brief Clear existing released callbacks associated to the given key
     *
     * @warning This will clear ALL existing callbacks
     *
     * @param key The key to delete associated callbacks
     */
    void clearReleasedKeyCallbacks(sf::Keyboard::Key key) noexcept;

    /**
     * @brief Do appropriate callbacks when a key is pressed
     *
     * @param key The key that get pressed
     *
     * @throw anything that can be thrown by the callback function
     */
    void keyPressed(sf::Keyboard::Key key);
    /**
     * @brief Do appropriate callbacks when a key is released
     *
     * @param key The key that get pressed
     *
     * @throw anything that can be thrown by the callbacks functions
     */
    void keyReleased(sf::Keyboard::Key key);

   private:
    /**
     * @var pressedKeys
     * @brief Keys list with associated callbacks
     */
    std::unordered_map<sf::Keyboard::Key, std::vector<utils::Callback>> pressedKeys;
    /**
     * @var releasedKeys
     * @brief Keys list with associated callbacks
     */
    std::unordered_map<sf::Keyboard::Key, std::vector<utils::Callback>> releasedKeys;
};

} // namespace graphic
