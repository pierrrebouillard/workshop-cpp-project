#pragma once

static constexpr unsigned short const PLAYER = 1;
static constexpr unsigned short const ENEMY = 2;
static constexpr unsigned short const PROJECTILE = 3;
