/**
 * @file Client.hpp
 * @brief The network abstraction class for the rtype network.
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include "SFML/Config.hpp"
#include "SFML/Network/Packet.hpp"
#include "SFML/System/String.hpp"

#include <SFML/Network.hpp>
#include <iostream>
#include <optional>

/**
 * @namespace network
 * @brief This is the main namespace for all the network in this RTypes project.
 */
namespace network {

/* @enum importance
 * @brief This enum class the importance of the data to send.
 */
enum importance {
    low = 0, /*!< Low importance of the data. Data send one time with no confirmation. */
    normal = 1, /*!< Normal importance of the data. Data send many time with no confirmation. */
    high = 2, /*!< High importance of the data. Data send until the receiver send confirmation. */
};

/*! \enum priority
 *
 * @brief This enum class all the data priority.
 */
enum priority {
    slow = 0, /*!< Slow priority, the data will be send at the end. */
    ordinary = 1, /*!< Ordinary priority, the data will be send when all fast priority are send. */
    fast = 2, /*!< Fast priority, the data will be send at the top of the queue. */
};

/**
 * @class Client
 * @brief Client class is made to communicate with the Server class.
 */
class Client {
   public:
    Client() noexcept = delete;
    Client(sf::IpAddress adressIp, unsigned short sPort);
    Client(Client const& other) noexcept = delete;
    Client(Client&& other) noexcept = delete;

    Client& operator=(Client const& other) noexcept = delete;
    Client& operator=(Client&& other) noexcept = delete;

    ~Client() noexcept;

   private:
    sf::IpAddress adressIp; /*!< The server Ipv4 adress*/
    unsigned short port; /*!< The client port */
    unsigned short sPort; /*!< The server port */
    sf::UdpSocket socket; /*!< The udp socket */
};
} // namespace network
