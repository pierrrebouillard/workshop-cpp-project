/**
 * @file GameScene.hpp
 * @author CORENTIN
 * @brief File for the game scene
 */

#include "Game/BorderWalls.hpp"
#include "Game/Bullet.hpp"
#include "Game/Enemy.hpp"
#include "Game/Explosion.hpp"
#include "Game/Parallax.hpp"
#include "Game/Player.hpp"
#include "GameEngine/Scene.hpp"

namespace scene {

/**
 * @class GameScene
 * @brief the class for the game scene
 */
class GameScene final : public ge::IScene {
   public:
    /**
     * @brief Construct a new Game Scene object
     */
    GameScene();
    GameScene(GameScene const& other) noexcept = delete;
    GameScene(GameScene&& other) noexcept = delete;
    ~GameScene() noexcept final = default;

    GameScene& operator=(GameScene const& other) noexcept = delete;
    GameScene& operator=(GameScene&& other) noexcept = delete;
    /**
     * @brief update the game scene
     *
     * @param elapsedTime
     */
    void update(float elapsedTime) final;

   private:
    /**
     * @var filepaths
     * @brief filepath for the parallax
     */
    std::vector<std::string> filepaths{
        std::string{"parallax/bg-1.png"},
        std::string{"parallax/bg-2.png"},
        std::string{"parallax/bg-3.png"}};
    /**
     * @var bulletManager
     * @brief the game BulletManager
     */
    game::BulletManager bulletManager;
    /**
     * @var explosionManager
     * @brief the game explosionManager
     */
    game::ExplosionManager explosionManager;
    /**
     * @var parallaxManager
     * @brief the game parallaxManager
     */
    game::ParallaxManager parallaxManager;
    /**
     * @var borderWalls
     * @brief the game borderWalls
     */
    game::BorderWalls borderWalls;
    /**
     * @var player
     * @brief the game player
     */
    game::Player player;
    /**
     * @var enemyManager
     * @brief the game enemyManager
     */
    game::EnemyManager enemyManager;
    /**
     * @var enemyCreator
     * @brief the game enemyCreator
     */
    game::EnemyCreator enemyCreator;
};

} // namespace scene
